$( document ).ready( function() {

  function CreateMultipleChoiceQuestion( index, questionInfo )
  {
    var quizHtml = "<div class='quiz-item' index='" + index + "' type='" + questionInfo["type"] + "'>";

    // Header
    quizHtml += "<div class='header'>- Question " + (index+1) + "</div>";

    quizHtml += "<div class='collapsable expanded'>";

    // Question
    quizHtml += "<div class='question'><p>" + questionInfo["question"] + "</p></div>";

    // Answers
    quizHtml += "<div class='answers row'>";

    var colWidth = questionInfo["width"];
    for ( var o = 0; o < questionInfo["options"].length; o++ )
    {
      var option = questionInfo["options"][o];

      quizHtml += "<div class='option col-md-" + colWidth + " fit-image'><div class='contents'>";
      quizHtml += "<input class='answer_button' type='radio' value='" + option["key"] + "' name='answer_" + index + "' id='answer_" + index + "-" + o + "'>";
      quizHtml += "<label for='answer_" + index + "-" + o + "'>" + option["text"] + "</label>";
      quizHtml += "</div></div>";
    }
    quizHtml += "</div> <!-- answers -->";
    quizHtml += "<input type='hidden' value='" + questionInfo["solution"] + "' class='solution'>";

    // More info
    quizHtml += "<div class='more-info'>";
    quizHtml += "<p>" + questionInfo["more-info"] + "</p>";
    quizHtml += "<ul>";
    for ( var r = 0; r < questionInfo["resources"].length; r++ )
    {
      var resource = questionInfo["resources"][r];

      quizHtml += "<li><a href='" + resource["url"] + "'>" + resource["name"] + "</a></li>";
    }
    quizHtml += "</ul>";
    quizHtml += "</div> <!-- more-info -->";

    // Grade just this question
    quizHtml += "<div class='tools'>";
    quizHtml += "<input type='button' value='Check just this question' class='check-question form-control'>";
    quizHtml += "</div>";

    // End of quiz-item
    quizHtml += "</div> <!-- collapsable -->";
    quizHtml += "</div> <!-- quiz-item -->";

    // Add to the quiz container
    $( "#quiz-container" ).append( quizHtml );
  }

  function GradeMultipleChoiceQuestion( obj )
  {
    console.log( "GradeMultipleChoiceQuestion" );

    var score = 0;
    var questionIndex = obj.attr( "index" );

    var selectedAnswer = "";
    obj.find( ".answer_button" ).each( function() {
      console.log( "ANSWERBTN", $( this ) );
      if ( $( this ).is( ":checked" ) )
      {
        selectedAnswer = $( this ).val();
      }
    } );

    var solution = obj.find( ".solution" ).val();
    
    console.log( "SELECTED:", selectedAnswer, "SOLUTION:", solution );
    
    obj.find( ".answers" ).removeClass( "correct" );
    obj.find( ".answers" ).removeClass( "incorrect" );
    
    obj.find( ".header" ).removeClass( "correct" );
    obj.find( ".header" ).removeClass( "incorrect" );
    
    if ( selectedAnswer == solution )
    {
      obj.find( ".answers" ).addClass( "correct" );
      obj.find( ".header" ).addClass( "correct" );
      score = 1;
    }
    else
    {
      obj.find( ".answers" ).addClass( "incorrect" );
      obj.find( ".header" ).addClass( "incorrect" );
    }
    
    obj.find( ".more-info" ).fadeIn( "fast", function() { } );

    return score;
  }

  function CreateDropdownQuestion( index, questionInfo )
  {
    var quizHtml = "<div class='quiz-item' index='" + index + "' type='" + questionInfo["type"] + "'>";

    // Header
    quizHtml += "<div class='header'>- Question " + (index+1) + "</div>";

    quizHtml += "<div class='collapsable expanded'>";
    
    // Question
    quizHtml += "<div class='question'><p>" + questionInfo["question"] + "</p></div>";

    // Answers
    quizHtml += "<div class='answers row'>";
    for ( var d = 0; d < questionInfo["dropdowns"].length; d++ )
    {
      var option = questionInfo["dropdowns"][d];

      quizHtml += "<div class='option col-md-" + option["width"] + "'><div class='contents'>";
      quizHtml += "<div class='row'>";
      quizHtml += "<div class='col-md-6'>" + option["prompt"] + "</div>";
      
      quizHtml += "<div class='col-md-6'>";
      quizHtml += "<select class='form-control dropdown' id='answer_" + index + "-" + d + "' solution='" + option["solution"] + "'>";
      for ( var o = 0; o < option["options"].length; o++ )
      {
        quizHtml += "<option value='" + option["options"][o] + "'>" + option["options"][o] + "</option>";
      }
      quizHtml += "</select> ";
      quizHtml += "</div>";
      quizHtml += "</div> <!-- row -->";
      quizHtml += "</div></div> <!-- option -->";
    }
    quizHtml += "</div> <!-- answers -->";

    // More info
    quizHtml += "<div class='more-info'>";
    quizHtml += "<p>" + questionInfo["more-info"] + "</p>";
    quizHtml += "<ul>";
    for ( var r = 0; r < questionInfo["resources"].length; r++ )
    {
      var resource = questionInfo["resources"][r];

      quizHtml += "<li><a href='" + resource["url"] + "'>" + resource["name"] + "</a></li>";
    }
    quizHtml += "</ul>";
    quizHtml += "</div> <!-- more-info -->";

    // Grade just this question
    quizHtml += "<div class='tools'>";
    quizHtml += "<input type='button' value='Check just this question' class='check-question form-control'>";
    quizHtml += "</div>";

    // End of quiz-item
    quizHtml += "</div> <!-- collapsable -->";
    quizHtml += "</div>";

    // Add to the quiz container
    $( "#quiz-container" ).append( quizHtml );
  }

  function GradeDropdownQuestion( obj )
  {
    console.log( "GradeDropdownQuestion" );

    var score = 0;
    var allCorrect = true;
    obj.find( ".dropdown" ).each( function() {
      var studentAnswer = $( this ).val();
      var solution = $( this ).attr( "solution" );

      $( this ).parent().parent().removeClass( "correct" );
      $( this ).parent().parent().removeClass( "incorrect" );
      
      obj.find( ".header" ).removeClass( "correct" );
      obj.find( ".header" ).removeClass( "incorrect" );

      if ( studentAnswer == solution )
      {
        $( this ).parent().parent().addClass( "correct" );
        obj.find( ".header" ).addClass( "correct" );
      }
      else
      {
        $( this ).parent().parent().addClass( "incorrect" );
        obj.find( ".header" ).addClass( "incorrect" );
        allCorrect = false;
      }
    } );

    obj.find( ".more-info" ).fadeIn( "fast", function() { } );

    if ( allCorrect ) { score = 1; }

    return score;
  }

  function CheckAnswers()
  {
    var totalCorrect = 0;

    $( ".quiz-item" ).each( function() {
      var type = $( this ).attr( "type" );
      var obj = $( this );

      if      ( type == "dropdown" )        { totalCorrect += GradeDropdownQuestion( obj ); }
      else if ( type == "multiple-choice" ) { totalCorrect += GradeMultipleChoiceQuestion( obj ); }
    } );


    $( "#insert-total-correct" ).html( totalCorrect );
    $( ".insert-total-correct" ).html( totalCorrect );
  }
  
  function CheckAnswer( obj )
  {
    var question = obj.closest( ".quiz-item" );
    var type = question.attr( "type" );
    
    if      ( type == "dropdown" )        { GradeDropdownQuestion( question ); }
    else if ( type == "multiple-choice" ) { GradeMultipleChoiceQuestion( question ); }
  }

  function InitQuestions()
  {
    for ( var i = 0; i < quizInfo.length; i++ )
    {
      if      ( quizInfo[i]["type"] == "dropdown" )         { CreateDropdownQuestion( i, quizInfo[i] ); }
      else if ( quizInfo[i]["type"] == "multiple-choice" )  { CreateMultipleChoiceQuestion( i, quizInfo[i] ); }
    }

    $( "#insert-total-questions" ).html( quizInfo.length );
    $( ".insert-total-questions" ).html( quizInfo.length );
    $( "#insert-total-correct" ).html( "0" );
    $( ".insert-total-correct" ).html( "0" );
  }

  function ToggleQuestion( obj )
  {
    var pane = obj.next( ".collapsable" );
    var text = obj.html();
    
    if ( text.includes( "-" ) )
    {
      // Collapse it
      pane.slideUp( "fast", function() { ; } );
      text = text.replace( "-", "+" );
    }
    else // +
    {
      // Expand it
      pane.slideDown( "fast", function() { ; } );
      text = text.replace( "+", "-" );
    }
    
    obj.html( text );    
  }
  
  function CollapseAll()
  {
    $( ".quiz-item" ).each( function() {      
      var text = $( this ).find( ".header" ).html();
        text = text.replace( "-", "+" );
      $( this ).find( ".header" ).html( text );
     
      $( this ).find( ".collapsable" ).each( function() {
        $( this ).fadeOut( "fast", function() { } );
      } );
    } );
  }
  
  function ExpandAll()
  {
    $( ".quiz-item" ).each( function() {      
      var text = $( this ).find( ".header" ).html();
        text = text.replace( "+", "-" );
      $( this ).find( ".header" ).html( text );
     
      $( this ).find( ".collapsable" ).each( function() {
        $( this ).fadeIn( "fast", function() { } );
      } );
    } );
  }

  // Init
  InitQuestions();

  // Callbacks
  $( ".btn-check-answers" ).click( function() {
    CheckAnswers();
  } );
  
  $( ".quiz-item .header" ).click( function() {
    ToggleQuestion( $( this ) );
  } );
  
  $( ".btn-collapse-all" ).click( function() {
    CollapseAll();
  } );
  
  $( ".btn-expand-all" ).click( function() {
    ExpandAll();
  } );
  
  $( ".check-question" ).click( function() {
    CheckAnswer( $( this ) );
  } );

} );
