$( document ).ready( function() {
  var sectionNumbers = ["350", "351", "376", "378"];
  
  var lectureList = [];
  var readingList = [];
  var exerciseList = [];
  var quizList = [];
  var techliteracyList = [];
  var projectList = [];
  var testList = [];
  
  // Compile list of each assignment type
  $.each( unitInfo, function( key, info ) {
    if ( key != "TEMPLATE" )
    {
      $.each( info["lectures"],       function( index, item ) { lectureList.push( item ); } );
      $.each( info["reading"],        function( index, item ) { readingList.push( item ); } );
      $.each( info["exercises"],      function( index, item ) { exerciseList.push( item ); } );
      $.each( info["quizzes"],        function( index, item ) { quizList.push( item ); } );
      $.each( info["tech-literacy"],  function( index, item ) { techliteracyList.push( item ); } );
      $.each( info["projects"],       function( index, item ) { projectList.push( item ); } );
      $.each( info["exams"],          function( index, item ) { testList.push( item ); } );
    }
    
  } ); // each
  
  console.log( lectureList );
  console.log( exerciseList );
  
  
  // Fill the cards with the assignments
  for ( var i = 0; i < lectureList.length; i++ )
  {
    $( "#lecture-list" ).append( "<li><a href='" + lectureList[i]["url"] + "'>" + lectureList[i]["name"] + "</a></li>" );
  }
  
  for ( var i = 0; i < readingList.length; i++ )
  {
    $( "#reading-list" ).append( "<li><a href='" + readingList[i]["url"] + "'>" + readingList[i]["name"] + "</a></li>" );
  }
  
  PopulateDocAssignment( exerciseList, "#exercise-list", "🏋️" );
  PopulateDocAssignment( projectList, "#project-list", "💻" );
  PopulateCanvasAssignment( quizList, "#quiz-list", "✅" );
  PopulateCanvasAssignment( techliteracyList, "#techliteracy-list", "🧠" );
  PopulateCanvasAssignment( testList, "#test-list", "💯" );
  
  function PopulateDocAssignment( list, id, symbol )
  {
    for ( var i = 0; i < list.length; i++ )
    {
      var row = "<tr><td>[" + list[i]["key"] + "]</td>";
        row += "<td>" + symbol + " " + list[i]["name"] + "</td>";
        if ( list[i]["docs"] !== undefined )
        {
          row += "<td><a href='" + list[i]["docs"] + "'>📋 Docs</a></td>";
        }
        
      for ( var s = 0; s < sectionNumbers.length; s++ )
      {
        var sec = sectionNumbers[s];
        if ( list[i][sec] !== undefined )
        {
          row += "<td><a href='" + list[i][sec] + "'><img src='../web-assets/graphics/canvas-icon.png'> " + sec + "</a></td>";
        }
      }
        
        // <td><a href=''>Docs</a></td><td><a href=''>Docs</a></td>";
      $( id ).append( row );
    }
  }
  
  function PopulateCanvasAssignment( list, id, symbol )
  {
    for ( var i = 0; i < list.length; i++ )
    {
      var row = "<tr><td>[" + list[i]["key"] + "]</td>";
        row += "<td>" + symbol + " " + list[i]["name"] + "</td>";
        
      for ( var s = 0; s < sectionNumbers.length; s++ )
      {
        var sec = sectionNumbers[s];
        if ( list[i][sec] !== undefined )
        {
          row += "<td><a href='" + list[i][sec] + "'><img src='../web-assets/graphics/canvas-icon.png'> " + sec + "</a></td>";
        }
      }
        
      $( id ).append( row );
    }
  }
  
} );
