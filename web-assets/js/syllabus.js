$( document ).ready( function()
{
  var syllabusHtml = "";
  console.log( headerInfo );
  
  $( ".insert_headerinfo_course" ).each( function()         {   $( this ).html( headerInfo['course'] ); } );
  $( ".insert_headerinfo_semester" ).each( function()       {   $( this ).html( headerInfo['semester'] ); } );
  $( ".insert_headerinfo_dates" ).each( function()          {   $( this ).html( headerInfo['course'] ); } );
  $( ".insert_prerequisites" ).each( function()             {   $( this ).html( headerInfo['prerequisites'] ); } );
  $( ".insert_description" ).each( function()               {   $( this ).html( headerInfo['description'] ); } );
  $( ".insert_credit_hours" ).each( function()              {   $( this ).html( headerInfo['credit_hours'] ); } );
  
  
  // Fill out the sections
  var sectionHtml = "";
  for ( var i = 0; i < sectionInfo.length; i++ )
  {
    var info = sectionInfo[i];
    
    sectionHtml += "<tr>";
    sectionHtml += "  <td>" + info["section"] + "</td>";
    sectionHtml += "  <td>" + info["crn"] + "</td>";
    sectionHtml += "  <td>" + info["method"] + "</td>";
    sectionHtml += "  <td>" + info["times"] + "</td>";
    sectionHtml += "  <td>" + info["room"] + "</td>";
    sectionHtml += "</tr>";
  }
  
  $( "#insert_sections" ).append( sectionHtml );
  
  $( "#insert_checkin_weight" ).html(   $("#weight_checkin").val() );
  $( "#insert_exercise_weight" ).html(  $("#weight_exercise").val() );
  $( "#insert_quiz_weight" ).html(      $("#weight_quiz").val() );
  $( "#insert_project_weight" ).html(   $("#weight_project").val() );
  $( "#insert_exam_weight" ).html(      $("#weight_exam").val() );
  $( "#insert_misc_weight" ).html(      $("#weight_misc").val() );
  
  
  
  // Insert schedule
  var scheduleHtml = "";
  for ( var i = 0; i < weekInfo.length; i++ )
  {
    scheduleHtml += "<tr>";
    scheduleHtml += "  <td>" + (i+1) + "</td>";
    
    var units = "";
    var topics = "";
    
    for ( var j = 0; j < weekInfo[i]["units"].length; j++ )
    {
      var unitKey = weekInfo[i]["units"][j];
      
      if ( unitKey.indexOf( "Unit" ) != -1 )
      {
          var unit = unitInfo[unitKey];
          units += unitKey + " ";
          topics += unitInfo[unitKey]["topic"];
      }
      else
      {
        units += " ";
        topics += unitKey + " ";
      }
    }
    
    scheduleHtml += "  <td>" + units + "</td>";
    scheduleHtml += "  <td>" + topics + "</td>";
    scheduleHtml += "</tr>";
  }
  $( "#insert_schedule" ).append( scheduleHtml );
  
                  
                  
} );

                  
