<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- <title> Introduction - Core Programming Concepts </title> -->
  <!-- <title> Introduction - Introduction to Data Structures </title> -->
  <title> Logic - Introduction to Propositional Logic - Discrete Math </title>
  
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="../web-assets/favicon.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../../web-assets/style/2020_10.css">
  <script src="../../web-assets/js/header-bar.js"></script>
  <script src="../../web-assets/js/utilities.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
        <input type="text" id="prev_page" value="blah1.html">
        <input type="text" id="prev_page_name" value="Chapter X">
        <input type="text" id="next_page" value="blah2.html">
        <input type="text" id="next_page_name" value="Chapter Y">
    </div> <!-- data -->

    <div class="container-fluid textbook exercise">
        <div id="course-links"></div> <!-- JS inserts links -->
        
        <div class="title row">
          <div class="col-md-8 chapter">
            <h1 id="class-info"> Propositional Logic: <br> Introduction to Propositional Logic </h1>
          </div>
          <div class="col-md-4 book">
            Discrete Math
          </div>
        </div>
        
        <a name="top">&nbsp;</a>
        
        <hr>
        
        <nav class="row booknav">
          <div class="col-md-4 prev"><a href="" class="insert-prev_page">&lt;&lt; <span class="insert-prev_page_name"></span></a></div>
          <div class="col-md-4 main"><a href="../index.html">Back to Index</a></div>
          <div class="col-md-4 next"><a href="" class="insert-next_page"><span class="insert-next_page_name"></span> &gt;&gt;</a></div>
        </nav>
        <hr>
        
        <!-- === BEGIN BOOK CONTENTS === -->
        <div class="row"><div class="col-md-12">
          
          <p>
            If you've done any programming so far, you might be familiar with
            working with if statements and while loops. These both work with
            <strong>propositional logic</strong> to decide whether to branch
            the program, or continue looping a set of instructions.
            <br><br>
            Learning about propositional logic can really help with writing
            better programming logic, and also help you debugging your logic.
          </p>
          
          <hr><h2>Propositions</h2>
          
          <div class="definition">
            A <strong>proposition</strong> is a statement that is
            <strong>true</strong> or <strong>false</strong>.
            Often, we think of these as "yes/no" questions.
          </div><br>
          
          <p>Some examples of propositional statements are:</p>
          <div class="row">
            <div class="col-md-3 framed pr-highlighted">Fran has a cat.</div>
            <div class="col-md-3 framed pr-highlighted">The cat is black.</div>
            <div class="col-md-3 framed pr-highlighted"><span class="math">x &gt; 10</span></div>
            <div class="col-md-3 framed pr-highlighted"><span class="math">x</span> and <span class="math">y</span> are equal</div>
          </div><br><br>
          
          <p>
            These are statements that can result in <strong>true</strong> (Fran DOES have a cat!) or
            <strong>false</strong> (Actually, the cat is orange). We could use these statements
            in a program, with an if statement...
          </p>
          
<pre><code class="cpp">string catColor;
cout &lt;&lt; "What color is the cat? ";
cin &gt;&gt; catColor;

if ( catColor == "black" )
{
  // proposition was true...
}
else
{
  // proposition was false...
}
</code></pre>
          
          <p>
            We can essentially think of it as a yes/no question of "Is the cat black?",
            where we could have different sets of operations to perform if the statement is true
            (the cat is black) or if the statement is false (the cat is not black).
          </p>
          
          <p>
            On the flip side, we <em>could not</em> write a program with questions like
            "What is your favorite color?" -- That is not a yes/no question, and the computer
            wouldn't understand what that type of question is. Instead, if we really wanted
            to define behavior based on favorite color, we would need a series of if / else if
            statements asking about "is favorite color red?", "is favorite color blue?", and so on...
          </p>

<pre><code class="cpp">string favColor;
cout &lt;&lt; "What is your favorite color? ";
cin &gt;&gt; favColor;

if ( favColor == "red" )
{
  // Code for red as favorite color...
}
else if ( favColor == "orange" )
{
  // Code for orange as favorite color...
}
else if ( favColor == "yellow" )
{
  // Code for yellow as favorite color...
}
// ... and so on ...
</code></pre>
  
          <!-- JS WIDGET -->
          <div class="js-widget" id="r1">   
            <div class="header">Self Review 1</div>         
            <script>
              $( document ).ready( function() {
                
                correct = "✔️";
                incorrect = "❌";
                
                r1_solutions = {
                  "r1_q1" : "r1_q1_AnswerA",
                  "r1_q2" : "r1_q2_AnswerA",
                  "r1_q3" : "r1_q3_AnswerB",
                  "r1_q4" : "r1_q4_AnswerA",
                };
                
                r1_notes = {
                  "r1_q1" : "This is both a proposition, and a TRUE statement.",
                  "r1_q2" : "This is a proposition, even though it is a FALSE statement.",
                  "r1_q3" : "This is NOT a proposition, since its result isn't TRUE or FALSE.",
                  "r1_q4" : "This is a proposition, and it is probably a FALSE statement. :)",
                };
                
                $( "#r1 .Answer" ).click( function() {
                    var question = $(this).attr( "question" );
                    var userAnswer = $(this).attr( "id" );
                    var correctAnswer = r1_solutions[question];
                    
                    console.log( "Question:", question, "User answer:", userAnswer, "Correct answer:", correctAnswer );
                    
                    $( "#" + question + "_Notes" ).html( r1_notes[question] );
                    if ( userAnswer == correctAnswer )
                    {
                      $( "#" + question + "_Result" ).html( correct );
                    }
                    else
                    {
                      $( "#" + question + "_Result" ).html( incorrect );
                    }
                  } );
              } );
            </script>
            <div class="viewable">
              Identify whether the following are propositions or not...
              
              <hr>
              <div class="row" id="r1_q1">
                <div class="col-md-1 col-sm-1"><p id="r1_q1_Result"></p></div>
                <div class="col-md-3 col-sm-11"><p>2 + 2 = 4</p></div>
                <div class="col-md-3 col-sm-12"><p id="r1_q1_Notes"></p></div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q1_AnswerA" class="Answer" question="r1_q1" value="Proposition" >
                </div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q1_AnswerB" class="Answer" question="r1_q1" value="Not a Proposition" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r1_q2">
                <div class="col-md-1 col-sm-1"><p id="r1_q2_Result"></p></div>
                <div class="col-md-3 col-sm-11"><p>2 + 3 = 6</p></div>
                <div class="col-md-3 col-sm-12"><p id="r1_q2_Notes"></p></div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q2_AnswerA" class="Answer" question="r1_q2" value="Proposition" >
                </div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q2_AnswerB" class="Answer" question="r1_q2" value="Not a Proposition" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r1_q3">
                <div class="col-md-1 col-sm-1"><p id="r1_q3_Result"></p></div>
                <div class="col-md-3 col-sm-11"><p>What movie do you want to see?</p></div>
                <div class="col-md-3 col-sm-12"><p id="r1_q3_Notes"></p></div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q3_AnswerA" class="Answer" question="r1_q3" value="Proposition" >
                </div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q3_AnswerB" class="Answer" question="r1_q3" value="Not a Proposition" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r1_q4">
                <div class="col-md-1 col-sm-1"><p id="r1_q4_Result"></p></div>
                <div class="col-md-3 col-sm-11"><p>This class has 60 students</p></div>
                <div class="col-md-3 col-sm-12"><p id="r1_q4_Notes"></p></div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q4_AnswerA" class="Answer" question="r1_q4" value="Proposition" >
                </div>
                <div class="col-md-2 col-sm-6">
                  <input type="button" id="r1_q4_AnswerB" class="Answer" question="r1_q4" value="Not a Proposition" >
                </div>
              </div> <!-- Question end -->
              
            </div>
          </div>
          <!-- JS WIDGET -->

          <hr><h2>Compound Propositions</h2>
          
          <p>
            We can also create a <strong>compound proposition</strong> using the logical operators AND, OR, and NOT.
          </p>
          
          <h5>AND ∧</h5>
          <p>
            In C++, Java, and C#, the <code>&amp;&amp;</code> symbol is used for a logical "AND".
            In discrete math, we often use <code>∧</code>.
          </p>
          <p>
            If we have two propositional statements, e.g., <span class="math">p</span>
            and <span class="math">q</span>, then the compound proposition
            <span class="math">p ∧ q</span>  is only <span class="t">true</span> when <strong>both sub-statements are also <span class="t">true</span>.</strong>
            If even one of the sub-statements are <span class="f">false</span>, then the entire compound statement is <span class="f">false</span>.
          </p>
          
          <div class="row">
            <div class="col-md-6 pr-highlighted">
              <P><strong>Fran has a cat AND Fran has a dog.</strong></P>
              
              <p>
                The entire compound proposition is true only if both sub-statements are true.
                This proposition asserts that Fran has both a cat and a dog at the same time,
                not just one or the other.
              </p>
            </div>
            <div class="col-md-6">              
              <table class="table truth-table">
                <tr><th>Sub-statement 1</th><th>Sub-statement 2</th><th style="border-left: solid;">Compound Proposition</th></tr>
                <tr><th>Fran has a cat</th><th>Fran has a dog</th><th style="border-left: solid;">Fran has a cat <br>AND Fran has a dog.</th><th>In English...</th></tr>
                <tr>
                  <td class="t">True</td>
                  <td class="t">True</td>
                  <td class="t" style="border-left: solid;">True</td>
                  <td>Fran has a cat. Fran has a dog.</td>
                </tr>
                <tr>
                  <td class="t">True</td>
                  <td class="f">False</td>
                  <td class="f" style="border-left: solid;">False</td>
                  <td>Fran has a cat. Fran does not have a dog.</td>
                </tr>
                <tr>
                  <td class="f">False</td>
                  <td class="t">True</td>
                  <td class="f" style="border-left: solid;">False</td>
                  <td>Fran does not have a cat. Fran has a dog.</td>
                </tr>
                <tr>
                  <td class="f">False</td>
                  <td class="f">False</td>
                  <td class="f" style="border-left: solid;">False</td>
                  <td>Fran does not have a cat. Fran does not have a dog.</td>
                </tr>
              </table>
            </div>
          </div>
          
          <div class="pr-newpage"></div>
          <h5>OR ∨</h5>
          <p>
            In C++, Java, and C#, the <code>||</code> symbol is used for a logical "OR".
            In discrete math, we often use <code>∨</code>.
          </p>
          <p>
            If we have two propositional statements, e.g., <span class="math">p</span>
            and <span class="math">q</span>, then the compound proposition
            <span class="math">p ∨ q</span>  is <span class="t">true</span> when <strong>one or both sub-statements are also <span class="t">true</span>.</strong>
            The result is only <span class="f">false</span> when <strong>all sub-statements are false.</strong>
          </p>
          
          <div class="row">
            <div class="col-md-6 pr-highlighted">
              <p><strong>Fran has a cat OR Fran has a dog.</strong></p>
              
              <P>
                The entire compound proposition is true if at least one sub-statement is true.
                This statement asserts that Fran has a cat or a dog. She could have one
                or the other, or she could have <em>both</em>, and all these scenarios result in a 
                <span class="t">true</span> outcome.
              </P>
            </div>
            <div class="col-md-6">              
              <table class="table truth-table">
                <tr><th>Sub-statement 1</th><th>Sub-statement 2</th><th style="border-left: solid;">Compound Proposition</th></tr>
                <tr><th>Fran has a cat</th><th>Fran has a dog</th><th style="border-left: solid;">Fran has a cat <br>OR Fran has a dog.</th><th>In English...</th></tr>
                <tr>
                  <td class="t">True</td>
                  <td class="t">True</td>
                  <td class="t" style="border-left: solid;">True</td>
                  <td>Fran has a cat. Fran has a dog.</td>
                </tr>
                <tr>
                  <td class="t">True</td>
                  <td class="f">False</td>
                  <td class="t" style="border-left: solid;">True</td>
                  <td>Fran has a cat. Fran does not have a dog.</td>
                </tr>
                <tr>
                  <td class="f">False</td>
                  <td class="t">True</td>
                  <td class="t" style="border-left: solid;">True</td>
                  <td>Fran does not have a cat. Fran has a dog.</td>
                </tr>
                <tr>
                  <td class="f">False</td>
                  <td class="f">False</td>
                  <td class="f" style="border-left: solid;">False</td>
                  <td>Fran does not have a cat. Fran does not have a dog.</td>
                </tr>
              </table>
            </div>
          </div>
          
          
          <div class="pr-newpage"></div>
          <h5>NOT ¬</h5>
          <p>
            In C++, Java, and C#, the <code>!</code> symbol is used for a logical "NOT".
            In discrete math, we often use <code>¬</code> (or sometimes <code>~</code>).
          </p>
          <p>
            NOT operates on one statement. Let's say we have the proposition <span class="math">p</span>.
            
            If <span class="math">p</span> is <span class="t">true</span>,
            then the result of <span class="math">¬p</span> is <span class="f">false</span>.
            
            If <span class="math">p</span> is <span class="f">false</span>,
            then the result of <span class="math">¬p</span> is <span class="t">true</span>.
          </p>
          
          <div class="row">
            <div class="col-md-6 pr-highlighted">
              <p><strong>It is NOT true that... The program is done.</strong></p>
              
              <P>
                Usually NOT is used as a "a is not equal to b" scenario...
<pre><code class="cpp">if ( a != b ) // ...</code></pre>
                
                
                but it can also be used to negate logical statements, such as if
                we wanted to keep running the program while "done" is not true...
<pre><code class="cpp">bool done = false;
while ( !done )   // While the program is NOT done...
{
  // ... then show main menu, process stuff, whatever ....
}
</code></pre>
              </P>
            </div>
            <div class="col-md-6">              
              <table class="table truth-table">
                <tr><th>Sub-statement 1</th><th style="border-left: solid;">Compound Proposition</th></tr>
                <tr><th>The program is done</th><th style="border-left: solid;">It is NOT true that... The program is done</th><th>In English...</th></tr>
                <tr>
                  <td class="t">True</td>
                  <td class="f" style="border-left: solid;">False</td>
                  <td>The program is done.</td>
                </tr>
                <tr>
                  <td class="f">False</td>
                  <td class="t" style="border-left: solid;">True</td>
                  <td>The program is not done.</td>
                </tr>
              </table>
            </div>
          </div>
          
          
          <!-- JS WIDGET -->
          <div class="js-widget" id="r2">   
            <div class="header">Self Review 2</div>         
            <script>
              $( document ).ready( function() {
                
                correct = "✔️";
                incorrect = "❌";
                
                r2_solutions = {
                  "r2_q1" : "r2_q1_AnswerB",
                  "r2_q2" : "r2_q2_AnswerA",
                  "r2_q3" : "r2_q3_AnswerB",
                  "r2_q4" : "r2_q4_AnswerA",
                };
                
                r2_notes = {
                  "r2_q1" : "In order for the compound statement to be TRUE, all sub-statements must be true.",
                  "r2_q2" : "Since this uses OR (∨), if at least one sub-statement is true, then the entire compound statement is true.",
                  "r2_q3" : "Since the printer is online, then &quot;Not online&quot; is false.",
                  "r2_q4" : "Since the printer is offline, then &quot;Not online&quot; is true.",
                };
                
                $( "#r2 .Answer" ).click( function() {
                    var question = $(this).attr( "question" );
                    var userAnswer = $(this).attr( "id" );
                    var correctAnswer = r2_solutions[question];
                    
                    console.log( "Question:", question, "User answer:", userAnswer, "Correct answer:", correctAnswer );
                    
                    $( "#" + question + "_Notes" ).html( r2_notes[question] );
                    if ( userAnswer == correctAnswer )
                    {
                      $( "#" + question + "_Result" ).html( correct );
                    }
                    else
                    {
                      $( "#" + question + "_Result" ).html( incorrect );
                    }
                  } );
              } );
            </script>
            <div class="viewable">
              Identify whether the following compound statements result in TRUE or FALSE...
              
              <hr>
              <div class="row" id="r2_q1">
                <div class="col-md-1 col-sm-1"><p id="r2_q1_Result"></p></div>
                <div class="col-md-6 col-sm-11">
                  <p>Printer is online = true,
                  <br>Printer has paper = false,
                  <br>Statement: Printer is online ∧ Printer has paper.
                  </p></div>
                <div class="col-md-3 col-sm-12"><p id="r2_q1_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q1_AnswerA" class="Answer" question="r2_q1" value="True" >
                </div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q1_AnswerB" class="Answer" question="r2_q1" value="False" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r2_q2">
                <div class="col-md-1 col-sm-1"><p id="r2_q2_Result"></p></div>
                <div class="col-md-6 col-sm-11">
                  <p>Printer is online = true,
                  <br>Printer has paper = false,
                  <br>Compound statement: Printer is online ∨ Printer has paper.
                  </p></div>
                <div class="col-md-3 col-sm-12"><p id="r2_q2_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q2_AnswerA" class="Answer" question="r2_q2" value="True" >
                </div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q2_AnswerB" class="Answer" question="r2_q2" value="False" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r2_q3">
                <div class="col-md-1 col-sm-1"><p id="r2_q3_Result"></p></div>
                <div class="col-md-6 col-sm-11">
                  <p>Printer is online = true,
                  <br>Compound statement: It is NOT true that... The printer is online.
                  </p></div>
                <div class="col-md-3 col-sm-12"><p id="r2_q3_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q3_AnswerA" class="Answer" question="r2_q3" value="True" >
                </div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q3_AnswerB" class="Answer" question="r2_q3" value="False" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r2_q4">
                <div class="col-md-1 col-sm-1"><p id="r2_q4_Result"></p></div>
                <div class="col-md-6 col-sm-11">
                  <p>Printer is online = false,
                  <br>Compound statement: It is NOT true that... The printer is online.
                  </p></div>
                <div class="col-md-3 col-sm-12"><p id="r2_q4_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q4_AnswerA" class="Answer" question="r2_q4" value="True" >
                </div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r2_q4_AnswerB" class="Answer" question="r2_q4" value="False" >
                </div>
              </div> <!-- Question end -->
              
            </div>
          </div>
          <!-- JS WIDGET -->
                    
          <hr>
          
          <h2>And now... A dramatic workplace play: The printer is offline - AND - The printer is out of paper.</h2>
          <br>
          
          <div class="row" style="width:80%; margin: 0 auto;">
            <div class="col-md-2 col-sm-1"><img src="images/av-jim.png" title="An icon of office worker Jim"></div>
            <div class="col-md-4 col-sm-5"><strong>Jim:</strong> Hey Janet, we can't print documents; the printer is offline AND the printer is out of paper.</div>

            <div class="col-md-4 col-sm-5"><strong>Janet:</strong> Alright Jim. Thanks for the heads up.</div>
            <div class="col-md-2 col-sm-1"><img src="images/av-janet.png" title="An icon of office worker Janet"></div>
                        
            <div class="col-md-12 col-sm-12"><p class="center">( ... later ... )</p></div>
            
            <div class="col-md-2 col-sm-1"><img src="images/av-rahaf.png" title="An icon of office worker Rahaf"></div>
            <div class="col-md-4 col-sm-5"><strong>Rahaf:</strong> Hey Janet, looks like the printer is offline, but it has paper.</div>

            <div class="col-md-4 col-sm-5"><strong>Janet:</strong> Has paper?? Jim lied to me!!</div>
            <div class="col-md-2 col-sm-1"><img src="images/av-janet2.png" title="An icon of office worker Janet"></div>
            
            <div class="col-md-12 col-sm-12"><p class="center">( ... later ... )</p></div>
            
            <div class="col-md-2 col-sm-1"><img src="images/av-rose.png" title="An icon of office worker Rose"></div>
            <div class="col-md-4 col-sm-5"><strong>Rose:</strong> Hey Janet, the printer is online but it's out of paper.</div>

            <div class="col-md-4 col-sm-5"><strong>Janet:</strong> Well, which is it?!?!</div>
            <div class="col-md-2 col-sm-1"><img src="images/av-janet3.png" title="An icon of office worker Janet"></div>
            
            <div class="col-md-12 col-sm-12"><p class="center">( ... later ... )</p></div>
            
            <div class="col-md-2 col-sm-1"><img src="images/av-anuraj.png" title="An icon of office worker Anuraj"></div>
            <div class="col-md-4 col-sm-5"><strong>Anuraj:</strong> I just printed out some docs... of course the printer is online and it has paper! </div>

            <div class="col-md-4 col-sm-5"><strong>Janet:</strong> That's the complete opposite!!</div>
            <div class="col-md-2 col-sm-1"><img src="images/av-janet4.png" title="An icon of office worker Janet"></div>
            
          </div>
          
          <hr><h2>Propositional variables</h2>
          
          <p>
            When working with propositional logic, we commonly shorten a propositional statement to a variable.
            For example, we can represent "she is in Discrete Math" with <span class="math">d</span>,
            and "she is majoring in Computer Science" with <span class="math">c</span>. We could then write out statements like:
          </p>
          
          <ul>
            <li><strong><span class="math">d ∧ c</span></strong><br>  She is in Discrete Math AND she is majoring in Computer Science.</li>
            <li><strong><span class="math">d ∧ ¬c</span></strong><br>  She is in Discrete Math AND she is NOT majoring in Computer Science.</li>
            <li><strong><span class="math">¬d ∧ c</span></strong><br>  She is NOT in Discrete Math AND she is majoring in Computer Science.</li>
            <li><strong><span class="math">¬d ∧ ¬c</span></strong><br>  She is NOT in Discrete Math AND she is NOT majoring in Computer Science.</li>
          </ul>
          
          <p>
            Propositional variables should be one letter in length. (though when programming, you should use longer variable names :)
          </p>
          
          
          <!-- JS WIDGET -->
          <div class="js-widget" id="r3">   
            <div class="header">Self Review 3</div>         
            <script>
              $( document ).ready( function() {
                                
                r3_notes = {
                  "r3_q1" : "p ∧ g",
                  "r3_q2" : "p ∧ ¬g",
                  "r3_q3" : "p ∨ g",
                  "r3_q4" : "p ∨ ¬g",
                };
                
                $( "#r3 .Answer" ).click( function() {
                    var question = $(this).attr( "question" );
                    var userAnswer = $(this).attr( "id" );
                                        
                    $( "#" + question + "_Notes" ).html( r3_notes[question] );
                  } );
              } );
            </script>
            <div class="viewable">
              Given the propositional variables <span class="math">p</span>: I am a pirate,
              and <span class="math">g</span>: I drink grog, and ∧ for AND, ∨ for OR, and ¬ for NOT,
              try translating the following into symbolic statements on paper. Click on "reveal" to see the solution.
              
              <hr>
              <div class="row" id="r3_q1">
                <div class="col-md-6 col-sm-11"><p>I am a pirate AND I drink grog.</p></div>
                <div class="col-md-4 col-sm-12"><p id="r3_q1_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r3_q1_AnswerA" class="Answer" question="r3_q1" value="Reveal" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r3_q2">
                <div class="col-md-6 col-sm-11"><p>I am a pirate AND I don't drink grog.</p></div>
                <div class="col-md-4 col-sm-12"><p id="r3_q2_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r3_q2_AnswerA" class="Answer" question="r3_q2" value="Reveal" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r3_q3">
                <div class="col-md-6 col-sm-11"><p>I am a pirate OR I drink grog.</p></div>
                <div class="col-md-4 col-sm-12"><p id="r3_q3_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r3_q3_AnswerA" class="Answer" question="r3_q3" value="Reveal" >
                </div>
              </div> <!-- Question end -->
              
              <hr>
              <div class="row" id="r3_q4">
                <div class="col-md-6 col-sm-11"><p>Either I am a pirate OR I don't drink grog.</p></div>
                <div class="col-md-4 col-sm-12"><p id="r3_q4_Notes"></p></div>
                <div class="col-md-1 col-sm-6">
                  <input type="button" id="r3_q4_AnswerA" class="Answer" question="r3_q4" value="Reveal" >
                </div>
              </div> <!-- Question end -->
              
            </div>
          </div>
          <!-- JS WIDGET -->
          
          <hr>
          <p>
            <br><br>
            (( Work in progres... ))
          </p>
          
          
          <hr>
          <div class="back-to-top">
            <a href="#top">Back to top</a>
          </div>
          <hr>
          <div class="license">
            <p>Written by Rachel Wil Sha Singh</p>
            <p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>
          </div>
        </div></div> <!-- <div class="row"> -->
        <!-- === END BOOK CONTENTS === -->

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
