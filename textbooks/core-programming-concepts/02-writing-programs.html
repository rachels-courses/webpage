<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Writing Programs - Core Programming Concepts - Chapter 2 </title>
  <!-- <title> Introduction - Intro to Data Structures </title> -->
  <!-- <title> Introduction - Discrete Math </title> -->
  
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="../../web-assets/favicon-cs200book.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../../web-assets/style/2020_10.css">
  <link rel="stylesheet" href="../../web-assets/style/2020_10_mobile.css">
  <script src="../../web-assets/js/header-bar.js"></script>
  <script src="../../web-assets/js/utilities.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
        <input type="text" id="prev_page" value="01-introduction.html">
        <input type="text" id="prev_page_name" value="Chapter 1: Introduction">
        <input type="text" id="next_page" value="03-variables-and-data-types.html">
        <input type="text" id="next_page_name" value="Chapter 3: Storing data with Variables">
    </div> <!-- data -->

    <div class="container-fluid textbook exercise">
        <div id="course-links"></div> <!-- JS inserts links -->
        
        <div class="title row">
          <div class="col-md-8 chapter">
            <h1 id="class-info"> Writing programs </h1>
          </div>
          <div class="col-md-4 book">
            Core Programming Concepts
          </div>
        </div>
        
        <a name="top">&nbsp;</a>
        
        <hr>
        
        <nav class="row booknav">
          <div class="col-md-4 prev"><a href="" class="insert-prev_page">&lt;&lt; <span class="insert-prev_page_name"></span></a></div>
          <div class="col-md-4 main"><a href="../index.html">Back to Index</a></div>
          <div class="col-md-4 next"><a href="" class="insert-next_page"><span class="insert-next_page_name"></span> &gt;&gt;</a></div>
        </nav>
        <hr>
        
        <!-- === BEGIN BOOK CONTENTS === -->
        <div class="row"><div class="col-md-12">
          
          <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
          
          <h2>A brief history of software</h2>
          
          <p>
            A computer needs to be told how to do <strong>everything</strong>.
            If you were writing a program totally from scratch, you would
            have to tell it how to load a bitmap file, how to draw a rectangle,
            how to detect mouse clicks, how to animate a transition, and everything else.
            <br><br>
            However, software has been evolving for decades now, and a lot of
            these features are already implemented in <strong>libraries</strong>.
            Libraries are sets of pre-written code meant for other programs to
            import and use.
            <br><br>
            With some of the first computers, the only commands you could
            program in directly mapped to the hardware on the machine
            (<a href="https://en.wikipedia.org/wiki/Machine_code">Machine code</a> / <a href="https://en.wikipedia.org/wiki/Assembly_language">Assembly code</a>). 
            Later, developers such as <a href="https://en.wikipedia.org/wiki/Grace_Hopper">Grace Hopper</a>
            worked on ways to write code that lets you fit multiple machine-code-pieces
            into one command that was more human readable, leading to early
            languages like <a href="https://en.wikipedia.org/wiki/Cobol">COBOL</a>.
            <br><br>
            Many of these "higher-level" languages (higher-level meaning
            further from the hardware; more abstracted) eventually will get
            turned into machine code through a process called <strong class="keyword">compiling</strong>.
          </p>

          <br>
          
          <p>
            For example, here's a simple C++ program that just writes "Hello, world!" to the screen:
          </p>
          
<pre><code class="cpp">#include &lt;iostream&gt;
using namespace std;

int main()
{
    cout << "Hello, world!" << endl;

    return 0;
}
</code></pre>          

          <p>And here is the corresponding assembly code:</p>
          
<pre><code class="asm">;5  :	{
0x5555555551a9	endbr64
0x5555555551ad	push   rbp
0x5555555551ae	mov    rbp,rsp
;6  :	    cout << "Hello, world!" << endl;
0x5555555551b1	lea    rsi,[rip+0xe4c]        # 0x555555556004
0x5555555551b8	lea    rdi,[rip+0x2e81]        # 0x555555558040 <std::cout@@GLIBCXX_3.4>
0x5555555551bf	call   0x555555555090 <std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)@plt>
0x5555555551c4	mov    rdx,rax
0x5555555551c7	mov    rax,QWORD PTR [rip+0x2e02]        # 0x555555557fd0
0x5555555551ce	mov    rsi,rax
0x5555555551d1	mov    rdi,rdx
0x5555555551d4	call   0x5555555550a0 <std::ostream::operator<<(std::ostream& (*)(std::ostream&))@plt>
;8  :	    return 0;
0x5555555551d9	mov    eax,0x0
;9  :	}
0x5555555551de	pop    rbp
0x5555555551df	ret</code></pre>
          
          <p>
            Compiled languages aren't the only variety - Java runs
            in a Java Virtual Machine, and Python is a scripting language
            that runs via a Python executable. But we're focusing on C++ here.
            <br><br>
            The point is, modern software is built on top of many layers:
            high level languages that compile down to machine code,
            pre-written libraries of code that handle common features
            for the programmers so they don't have to reinvent the wheel.
          </p>
        
          <hr><h2>What is a program?</h2>
       
          <p>
            Since a computer needs to be told how to do everything,
            a computer program is a list of very specific instructions
            on how to execute some task (or tasks, for larger programs).
          </p>
          
          <p class="center">
            <img src="images/inputoutput.png" title="">
          </p>
        
        <h3>Inputs:</h3>
        
        <p>
          Some programs don't take any <strong>input</strong> and just run
          a set of pre-defined instructions.
          Most programs do take some form of input, however, whether
          that's when the program first starts up or during its runtime.
          <br><br>
          Inputs could include things like passing in a filename
          to a program (e.g., please open this file) or other pieces of data,
          or getting keyboard input, gamepad input, mouse input, touch screen input,
          or receiving signals via the network or internet.
        </p>
        
        <h3>Outputs:</h3>
        
        <p>
          Programs also often will return some form of <strong>output</strong>,
          but this is also optional. If a program doesn't return output,
          maybe the user just wants to tell the program to do a job, but
          doesn't need confirmation that the job was done (these are usually
          called background processes).
          <br><br>
          Outputs could be something like displaying a message box,
          changing text on the screen, playing a sound, or writing out a text file.
        </p>
        
        <h3>Variables:</h3>
        
        <div class="row">
          <div class="col-md-8">
            <p>
              Our program may also use <strong class="keyword">variables</strong> to store data during
              its execution. Variables are locations in memory (RAM) where we can
              store numbers, text, or more complex objects like an image.
              We give variables a name to reference it by, such as <code>userName</code>
              or <code>cartTotal</code>, and we can do math operations on it,
              as well as write it to the screen or read new values into it from
              the user.
            </p>
          </div>
          <div class="col-md-4 fit-image">
            <img src="images/variables.png" title="">
          </div>
        </div>
        
        <h3>Branching and looping:</h3>
        
        <div class="row">
          <div class="col-md-10">
            <p>
              We can also change the instructions our program runs based on
              some <strong class="keyword">condition</strong> we set. For example, if <code>bankBalance &lt; 0</code>
              then maybe we display an error message. Otherwise, we can withdraw
              some <code>amount</code> of money from the <code>bankBalance</code>.
            </p>
          </div>
          <div class="col-md-2 fit-image">
            <img src="images/branching.png" title="">
          </div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-10">
            <p>
              We may also want to take advantage of <strong class="keyword">looping</strong> to do
              a set of instructions repeatedly, possibly with some variables
              changing each time through the loop. An example for this could be
              to loop over all of a student's <code>assignmentGrades</code>,
              adding up all the points so we can find an average.
            </p>
          </div>
          <div class="col-md-2 fit-image">
            <img src="images/looping.png" title="">
          </div>
        </div>
        
        
        <h3>Example programs</h3>
        
        <h5>Area calculator:</h5>
<pre><code class="cpp">cout &lt;&lt; "Width: ";      // Display message
cin &gt;&gt; width;           // Get user input

cout &lt;&lt; "Height: ";     // Display message
cin &gt;&gt; height;          // Get user input

area = width * height;  // Do math

cout &lt;&lt; "Area: " &lt;&lt; area &lt;&lt; endl;</code></pre>
        
        <h5>Recipe program:</h5>
<pre><code class="cpp">cout &lt;&lt; "How many batches? "    // Display message
cin &gt;&gt; batches;                 // Get user input

flour       = 2.75 * batches;   // Calculate amounts
bakingSoda  = 1 * batches;
butter      = 1 * batches;
whiteSugar  = 1.5 * batches;
eggs        = 1 * batches;
vanilla     = 1 * batches;

cout &lt;&lt; flour &lt;&lt; " cups flour" &lt;&lt; endl;
cout &lt;&lt; bakingSoda &lt;&lt; " tsp baking soda" &lt;&lt; endl;
cout &lt;&lt; butter &lt;&lt; " cups butter" &lt;&lt; endl;
cout &lt;&lt; whiteSugar &lt;&lt; " cups white sugar" &lt;&lt; endl;
cout &lt;&lt; eggs &lt;&lt; " eggs" &lt;&lt; endl;
cout &lt;&lt; vanilla &lt;&lt; " tsp vanilla" &lt;&lt; endl;</code></pre>
        
        <h5>ATM - Withdrawing money:</h5>
<pre><code class="cpp">cout &lt;&lt; "Withdraw: ";       // Display message
cin >> withdrawAmount;      // Get user input

if ( withdrawAmount > bankBalance )
    cout &lt;&lt; "Error! Not enough money." &lt;&lt; endl;
else
    bankBalance = bankBalance - withdrawAmount;</code></pre>
        
        
        
          <hr><h2>C++ programs</h2>
          
          <p>
            Each programming language is a little different in how it looks,
            but whether you're using C++, Java, C#, Python, or many other languages,
            you'll still encounter branching with if statements, looping,
            functions, and classes. (Though older languages may not have
            classes at all, like C!)
          </p>
          
          <h3><code>main()</code>: The starting point</h3>
        
          <p>
            With C++, Java, and C#, programs must begin in a function
            called <code>main()</code>. The compiler (that turns the program
            into machine code) is always expecting <code>main()</code> to be there and to
            begin running the program at the top of it.
          </p>
          
          <div class="row">
            <div class="col-md-4">
              <p><strong>Empty C++ program:</strong></p>
<pre><code class="cpp">int main()
{
    // Code goes here
    return 0;
}</code></pre>
            </div>
            <div class="col-md-4">
              <p><strong>Empty C# program:</strong></p>
<pre><code class="cs">class Program
{
    static void Main()
    {
        // Code goes here
    }
}</code></pre>
            </div>
            <div class="col-md-4">
              <p><strong>Empty Java program:</strong></p>
<pre><code class="java">public class Program
{
    public static void main()
    {
        // Code goes here
    }
}</code></pre>
            </div>
          </div>
          
          
          <p>
            In C++, the program starts at the opening curly-brace <code>{</code>,
            and the program ends at <code>return 0;</code>.
          </p>

          <p>
            The <code>return 0;</code> is an old way of saying "there were no errors!";
            old programs used to use number codes for different kinds of errors
            to make it easier for programmers to find problems. However, it's
            not much help to the user to see a cryptic message like
            "Error code 1832" before a program closes.
            But, it's still standard in C++ to end <code>int main()</code> with
            <code>return 0;</code>.
            <br><br><sub>(In some C++ compilers you can use
            <code>void main()</code> and avoid the return, but not all compilers
            accept this. Since it's not standard, you shouldn't do it.)</sub>
          </p>
        
          <h3>Basic C++ syntax</h3>

            <p>
              In C++, Java, C#, and other C-like langaguges there are
              some basic rules of syntax you should know about:
            </p>
            
            <h5>Lines of code:</h5>
            
            <p>
              A code statement ends with a semi-colon.
              Statements are single commands like <code>cout</code> ("console-out")
              to display text to the output, <code>cin</code> ("console-in")
              to read input from the keyboard, declaring variables, assigning variables,
              or doing math operations.
            </p>

<pre><code class="cpp">string state;            // Declare variable
cout &lt;&lt; "Enter state: "; // Display text
cin >> state;            // Get input from user
cout &lt;&lt; state &lt;&lt; endl;   // Display text stored in state</code></pre>
    
    
            <h5>Variable names:</h5>
            <p>
              There are some rules for naming variables in C++. Generally,
              you can use any upper or lower case letters, numbers, and underscores
              in variable names - no spaces allowed. Beyond that,
              variables cannot begin with a number, and you cannot use
              a <strong>keyword</strong> (such as <code>if</code>) as a variable name.
            </p>
            
            <h5>Code blocks:</h5>
            <p>
              There are certain types of instructions that <em>contain</em>
              additional code. If statements, for example, contain a set
              of instructions to execute <em>only if</em> its <strong>condition</strong>
              evaluates to <code>true</code>.
              <br><br>
              Any time an instruction <em>contains</em> other instructions,
              we use opening and closing curly braces <code>{ }</code> to
              contain this internal code.
              <br><br>
              Additionally, with these instructions, they <strong>do not</strong>
              end with semicolons.
            </p>
            
            <h5>Example program:</h5>
            
            <p>
              With this program, the text "Can't vote!" is contained within
              the <code>if ( age < 18 )</code> instruction and is only executed if
              the variable <code>age</code> contains a value less than 18.
              Otherwise (when <code>age</code> contains a value equal to or greater
              than 18), it will display the text "Can vote!".
            </p>
            
<pre><code class="cpp">if ( age &lt; 18 )
{
    cout &lt;&lt; "Can't vote!";
}
else
{
    cout &lt;&lt; "Can vote!";
}</code></pre>
            
            <h5>Comments:</h5>
            <p>
              It is often useful to add comments to programs to specify
              what is going on in the code. There are two ways to add
              comments in C++.
              <br><br>
              Using <code>//</code> allows you to add a one-line comment,
              for little notes.
            </p>
            
<pre><code class="cpp">// Calculate the area
area = width * height;</code></pre>

            <p>
              You can also write a multi-line comment. These comments
              must begin with <code>/*</code> and end with <code>*/</code>.
              This can be useful to add additional information in
              more depth, or documenting how your program works.
            </p>

<pre><code class="cpp">/*
This program calculates the area of a rectangle.
The user must enter a width and height that
is greater than 0, otherwise an error message
will be displayed.
*/</code></pre>

            <h5>Whitespace and code cleanliness:</h5>
            <p>
              Generally, C++ doesn't care if multiple commands are on
              one line or several lines. The compiler is going to compile
              it all down either way.
              <br><br>
              You should, however, care about the code's readability to humans.
              Add enough new lines to separate sections of a program,
              use consistent <strong class="keyword">indentation</strong>, and give variables
              descriptive names.
            </p>

            <p>
              When writing code inside of a code block, you should always tab forward internal code by one tab:
            </p>

<pre><code class="cpp">if ( order == "beer" )
{
    // One tab forward
    cout &lt;&lt; "Order: beer" &lt;&lt; endl;
    
    if ( age &gt;= 21 )
    {
        // One more tab forward
        cout &lt;&lt; "You get beer!";
    }
}</code></pre>


            <hr><h2>Diagnosing, testing, and debugging</h2>
            
            <div class="row">
              <div class="col-md-11">
                <p>
                  Software is an amorphous, intangible thing of arbitrary complexity.
                  <br><br>
                  It's bad enough when you're working alone, but once you get other
                  people involved at varying skill levels (and varying levels of
                  writing clean code), the amount of potential issues can quickly
                  skyrocket.
                  <br><br>
                  There are techniques to writing software that can help you validate
                  your own logic and check for errors before they occur, as well
                  as tools to help you diagnose and debug issues, as well as error
                  messages to also help give hints to what's going wrong.
                  All of these things are important to becoming a good software developer.
                </p>
              </div>
              <div class="col-md-1 fit-image">
                <img src="images/debugging.png" title="">
              </div>
            </div>

            <h3>Synax, Runtime, and Logic errors</h3>
            
            <h5>Syntax errors:</h5>
            <p>
              The best type of error you can get is a <strong class="keyword">syntax error</strong>,
              even though it may feel bad to get them. Syntax errors are when
              you have a typo or have otherwise miswritten a statement in the
              code, and the compiler doesn't know what to do with it. It will
              then display a <strong class="keyword">build error</strong> telling you what it needs.
              <br><br>
              Diagnosing syntax error messages can be confusing at first:
              often the compiler doesn't know exactly what's wrong, just
              what it's expecting.
              <br><br>
              But, the good thing about syntax errors is that your program
              won't run if it can't build - that means you <em>must</em>
              fix these errors before continuing. It also means that
              these errors can't just be hidden in the code, like other
              error types.
              <br><br>
              Since syntax errors are caught by the compiler,
              they're called <strong>compile-time errors</strong>.
            </p>
            
            <h5>Logic errors:</h5>
            <p>
              Another type of error that's much harder to track down are
              <strong class="keyword">logic errors</strong>. Logic errors can be errors in formulas,
              bad if statement conditions, or other things that don't do
              what the programmer was intending to do (again, either because
              of a typo, or not quite understanding the program flow, or
              for other reasons).
              <br><br>
              Logic errors don't show up in the error list, but can cause
              your program to crash down the line - or worse, never crash
              but create bad output. Because it's not crashing, you may
              assume everything is working fine, but incorrect output
              can cause problems.
            </p>
            
            <h5>Runtime errors vs. Compile-time errors</h5>
            <p>
              When a logic error causes the program to crash while it's running, it is called a <strong class="keyword">runtime error</strong>.
            </p>
            <p>
              Errors caught by the compiler (syntax errors) are compile-time errors.
              (They may also be called Build Errors.)
            </p>
        
        
            <h3>Debugging</h3>
            <div class="row">
              <div class="col-md-10">
                <p>
                  At some point, you will need to debug a program that
                  builds and runs, but something is just <em>wrong</em>.
                  Perhaps it crashes, or perhaps it just doesn't do what you
                  expect it to.
                  <br><br>
                  We will learn about how to use <strong>debugging tools</strong> in IDEs like
                  Visual Studio and Code::Blocks later on. Some of the tools
                  included in a debugger are:
                </p>
                
                <ul>
                  <li><strong>Breakpoints:</strong> You can set these in your code
                        and when the program reaches that part it will pause
                        execution. Then, you can investigate the values of
                        different variables, and also keep stepping forward
                        line-by-line to observe changes and program flow.</li><br>
                  <li><strong>Watch windows:</strong> You type in variable names
                        in these windows and it will show you the variable
                        values at different points in the program (using breakpoints).</li><br>
                  <li><strong>Call stack:</strong> This will show which functions
                        have been called up until where you're paused in the program.
                        This can be handy to see how the program flows between
                        different functions.</li>
                </ul>
              </div>
              <div class="col-md-2 fit-image">
                <img src="images/debugging2.png">
              </div>
            </div>
        
            <h5>Output here, there, everywhere:</h5>
            
            <p>
              A common, though somewhat unsophisticated debugging technique,
              is to add output statements at various points in your program
              to essentially "trace" the program flow - what is getting executed?
              What is displayed <em>before</em> the program crashes?
              <br><br>
              This technique takes more cleaning up after you're done,
              and is only so helpful, but it can be a good way to see
              how your program is running.
            </p>
        
            <h3>Testing</h3>
            <p>
              Programs need to be tested. Starting off, you will probably
              be manually running the program, entering inputs, and checking outputs.
              However, this gets tedious after a while and you'll probably
              start entering gibberish as inputs and assuming everything works
              if it outputs <em>something</em>.
              Manual testing is good to do, but it is easy to become sloppy.
              <br><br>
              You can also write code to automatically test certain things
              for you. This will become more relevant to us once we're working
              with functions in C++. 
              <br><br>
              The main idea behind testing is that programs or functions
              will be taking some sort of <strong>inputs</strong>, and given those
              inputs you have some <strong>expected outputs</strong> or results.
              If the <strong>actual outputs</strong> don't match the expected outputs,
              then there is something wrong.
              <br><br>
              We will cover testing techniques more later on.
            </p>
            
            <h3>Coding techniques for newbies</h3>
            <p>
              When you're first starting with C++, it can feel like
              your code just <em>never works</em>. It can be good to
              adopt a few of these practices while you're still new,
              to minimize errors and frustration:
            </p>
            <ul>
              <li><strong>Compile often:</strong> Get into the habit of
                        using keyboard shortcuts to build your code after
                        every few lines - especially when you're first
                        starting out. If you write two lines of code
                        and get a syntax error, it's much easier to diagnose
                        than if you write a whole program and then build.</li><br>
              <li><strong>Search for syntax error messages:</strong> Most of
                        these messages have been posted about before on
                        boards like Stack Overflow. Reading through these
                        can help give you insight into what the error
                        <em>means</em>, and common ways to fix them.</li><br>
              <li><strong>Comment things out:</strong> If you have a lot of
                        code but can't get it compiling and aren't sure what
                        to do, you can comment out large chunks of the program
                        until it builds again. Then, bit by bit, uncomment out
                        different regions until you find what's causing the build error.</li><br>
            </ul>
        
            <div class="warning">
              The most important thing is that your program can <strong>build</strong>! If your program doesn't build,
              then it's not a program. First and foremost, make sure your program builds.
            </div>
          
          
          
          <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
          
          <div class="back-to-top">
            <a href="#top">Back to top</a> <br> <a href="../index.html">Back to index</a>
          </div>
          <hr>
          
          <div class="license">
            <p>Written by Rachel Wil Sha Singh</p>
            <p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>
          </div>
        </div></div> <!-- <div class="row"> -->
        <!-- === END BOOK CONTENTS === -->

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
