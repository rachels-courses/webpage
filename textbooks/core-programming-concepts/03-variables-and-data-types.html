<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Variables and Data Types - Core Programming Concepts - Chapter 3 </title>
  <!-- <title> Introduction - Intro to Data Structures </title> -->
  <!-- <title> Introduction - Discrete Math </title> -->
  
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="../../web-assets/favicon-cs200book.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../../web-assets/style/2020_10.css">
  <script src="../../web-assets/js/header-bar.js"></script>
  <script src="../../web-assets/js/utilities.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
        <input type="text" id="prev_page" value="02-writing-programs.html">
        <input type="text" id="prev_page_name" value="Chapter 2: Writing programs">
        <input type="text" id="next_page" value="blah2.html">
        <input type="text" id="next_page_name" value="Chapter Y">
    </div> <!-- data -->

    <div class="container-fluid textbook exercise">
        <div id="course-links"></div> <!-- JS inserts links -->
        
        <div class="title row">
          <div class="col-md-8 chapter">
            <h1 id="class-info"> Storing data with Variables </h1>
          </div>
          <div class="col-md-4 book">
            Core Programming Concepts
          </div>
        </div>
        
        <a name="top">&nbsp;</a>
        
        <hr>
        
        <nav class="row booknav">
          <div class="col-md-4 prev"><a href="" class="insert-prev_page">&lt;&lt; <span class="insert-prev_page_name"></span></a></div>
          <div class="col-md-4 main"><a href="../index.html">Back to Index</a></div>
          <div class="col-md-4 next"><a href="" class="insert-next_page"><span class="insert-next_page_name"></span> &gt;&gt;</a></div>
        </nav>
        <hr>
        
        <!-- === BEGIN BOOK CONTENTS === -->
        <div class="row"><div class="col-md-12">
          
            <p class="center"><img src="images/variables.png"></p>
          
          
            <p>
                We use variables in programming as places to temporarily store data
                so that we can manipulate that data. We can have the user write
                to it via the keyboard, we can use files to write data to variables,
                we can do math operations or modify these variables, and we can
                print them back out to the screen or a text file.
            </p>
            
            <p><sub>
                (Printing here meaning "display on the screen"; not to a printer. 
                Holdover from ancient times when computers' outputs came via the printers.)
            </sub></p>
            
            <p>
                When we're writing programs in C++, we need to tell our program
                what the <strong><a href="https://en.wikipedia.org/wiki/Data_type">data type</a></strong> 
                of each variable is, and give each variable
                a <strong>variable name</strong> (aka identifier). When a variable is <strong>declared</strong>,
                a space in RAM is allocated to that variable and it can store its data there.
                That data can be manipulated or overwritten later as needed in the program.
            </p>
            
            <hr><h2>Data types</h2>
            
            <p>
                In C++, when we want to create a variable we need to tell the \textbf{compiler}
                what the <strong>data type</strong> of the variable is. Data types specify
                <em>what</em> is stored in the variable (whole numbers? numbers with decimal values?
                text? true/false values?). 
                Each data type take up a different amount of space in memory.
            </p>
            
            <table class="table">
                <tr><th>Data type</th><Th>Values</Th><th>Size</th><th>Example code</th></tr>
                <tr> <!-- integer -->
                    <td> <!-- data type -->
                        Integer (int)
                    </td>
                    <td> <!-- values -->
                        Whole numbers
                    </td>
                    <td> <!-- size -->
                        4 bytes
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">int age = 100;</code></pre>
                    </td>
                </tr>
                <tr> <!-- char -->
                    <td> <!-- data type -->
                        Character (char)
                    </td>
                    <td> <!-- values -->
                        Single symbols - letters, numbers, anything.
                    </td>
                    <td> <!-- size -->
                        1 byte
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">char currency = '$';</code></pre>
                    </td>
                </tr>
                <tr> <!-- float -->
                    <td> <!-- data type -->
                        Float
                    </td>
                    <td> <!-- values -->
                        Numbers with decimals
                    </td>
                    <td> <!-- size -->
                        4 bytes
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">float price = 1.95;</code></pre>
                    </td>
                </tr>
                <tr> <!-- double -->
                    <td> <!-- data type -->
                        Double
                    </td>
                    <td> <!-- values -->
                        Numbers with decimals
                    </td>
                    <td> <!-- size -->
                        8 bytes
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">double price = 1.95;</code></pre>
                    </td>
                </tr>
                <tr> <!-- boolean -->
                    <td> <!-- data type -->
                        Boolean (bool)
                    </td>
                    <td> <!-- values -->
                        true or false
                    </td>
                    <td> <!-- size -->
                        1 byte
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">bool saved = false;</code></pre>
                    </td>
                </tr>
                <tr> <!-- string -->
                    <td> <!-- data type -->
                        String
                    </td>
                    <td> <!-- values -->
                        Any text, numbers, symbols, any length.
                    </td>
                    <td> <!-- size -->
                        differs
                    </td>
                    <td> <!-- example code -->
                        <pre><code class="cpp">string password = "123secure";</code></pre>
                    </td>
                </tr>
            </table>
            
            <p>
                Of these, notice that the <strong>string</strong> data type doesn't have a fixed size. This
                is because technically, behind-the-scenes, a string is really just an 
                array (or list) of <code>char</code> data types. The C++ standard library contains a
                string library that handles a lot of text-related functionality like <code>find</code>.
                <br><br>
                <strong>Floats</strong> and <strong>doubles</strong> both store numbers with fractional (decimal) parts,
                but a double takes up <em>double the memory in RAM</em>, allowing to store
                more accurate fractional amounts. A <code>float</code> has 6 to 9 digits of precision
                and a <code>double</code> has 15 to 18 digits of precision.
                <br>
                <sub>(From https://www.learncpp.com/cpp-tutorial/floating-point-numbers/)</sub>
                <br><br>
                <strong>Boolean</strong> data types store <code>true</code> and <code>false</code> values,
                but will also accept integer values when assigned. It will convert
                a value of <code>0</code> to false and any other number to <code>true</code>.
            </p>
            
            <hr><h2>Declaring variables and assigning values to variables</h2>
            
            <h3>Variable declarations</h3>
            <p>When we're <strong>declaring</strong> a variable it needs to follow one of these formats:</p>
            
            <div class="textbook-code-form">
                <h5>Variable declaration formats</h5>
                <ul>
                    <li><code>DATATYPE VARIABLENAME;</code></li>
                    <li><code>DATATYPE VARIABLENAME = VALUE;</code></li>
                    <li><code>DATATYPE VARIABLENAME1, VARIABLENAME2, VARIABLENAME3;</code></li>
                    <li><code>DATATYPE VARIABLENAME1 = VALUE1, VARIABLENAME2 = VALUE2;</code></li>
                </ul>
            </div>
            
            <p>
                The <strong>data type</strong> goes first, then the <strong>variable name/identifier</strong>,
                and if you'd like, you can also <strong>assign a value</strong> during the same step
                (though this is not required).
                <br><br>
                Once a variable has been <strong>declared</strong>, you don't need to declare it again.
                This means you don't need to re-specify its data type when you're using it.
                Just address the variable by its name and that's all.
            </p>
            
            <h5>Example code - Code that uses integers to figure out how many candies each kid should get:</h5>
            
<pre><code class="cpp">// Declaring variables and assigning values
int totalCandies = 10;
int totalKids = 5;
int candiesPerKid = totalCandies / totalKids;

// Reusing the same variables later on
totalCandies = 100;
totalKids = 10;
candiesPerKid = totalCandies / totalKids;
</code></pre>
            
            <h5>Example code - Code to display the price plus tax to the user:</h5>
            
<pre><code class="cpp">// Declaring variables and assigning values
float price = 9.99;
float tax = 0.11;
// Text output to the screen. Can do math within!
cout &lt;&lt; "Please pay " &lt;&lt; (price + price * tax);
</code></pre>
            
            <br><br>
            <h3>Variable assignment</h3>
            <div class="textbook-code-form">
                <h5>Variable declaration formats</h5>
                <ul>
                    <li><code>VARIABLENAME = LITERAL;</code>
                    <br>(This stores the LITERAL value in the VARIABLENAME.)
                    </li>
                    <li><code>VARIABLENAME1 = VARIABLENAME2;</code>
                    <br>(This takes whatever is stored in VARIABLENAME2 and copies it to VARIABLENAME1.)
                    </li>
                </ul>
            </div>
                
            <p>
                When assigning a <strong>value</strong> to a <strong>variable</strong>,
                the variable being assigned to always goes on the left-hand side ("LHS")
                of the equal sign <code>=</code>.
                The <code>=</code> sign is known here as the <strong>assignment operator</strong>.
                <br><br>
                The item on the right-hand side ("RHS") will be the value
                stored in the variable specified on the LHS. This can be a
                <strong>literal</strong> (a hard-coded value), or it can be a different
                variable of the same data type, whose value you want to copy over.
            </p>
            
            <h5>Example code - Assigning literal values to variables:</h5>
<pre><code class="cpp">price = 9.99;       // 9.99 is a float literal
state = "Kansas";   // "Kansas" is a string literal
operation = '+';    // '+' is a char literal
</code></pre>
          
            <h5>Example code - Copying <code>student13</code>'s value to the <code>bestStudent</code> variable:</h5>
<pre><code class="cpp">bestStudent = student13;
</code></pre>

            <p class="center"><img src="images/assignment1.png"></p>
            
            <br><br>
            <h3>Naming conventions</h3>
          
            <p>
                You can name a variable anything you'd like - but it can only
                contain numbers, underscore (<code>_</code>), and upper- and lower-case letters
                in the name. Variable names can begin with the underscore
                or a letter, but it cannot start with a number. And definitely
                NO spaces allowed in a variable name! 
                <br><br>
                Additionally, a variable name cannot be a <strong>keyword</strong>
                in C++ - a name reserved for something else in the language,
                such as <code>void</code>, <code>if</code>, <code>int</code>, etc.
                <br><br>
                Everything in C++ is <strong>case sensitive</strong> as well, which means
                if you name a variable <code>username</code>, it will be a different
                variable from one named <code>userName</code> (or if you type
                "userName" when the variable is called "username", the compiler
                will complain at you because it doesn't know what you mean).
                <br><br>
                In C++, it's standard to name your variables using <strong>camelCasing</strong>,
                with a lower-case first letter. This means that if your variable
                name "should" contain spaces, you just capitalize the next word instead:
            </p>
            
<pre><code class="cpp">int howManyCookies;
string mySecurePassword;
float howMuchStudentLoansIHave;
</code></pre>
            
            <h3>Unsigned data types</h3>
        
            <p>
                Sometimes it doesn't make sense to store negative values in
                a variable - such as speed (which isn't directional, like velocity)
                or the size of a list (can't have negative items) or
                measurement (can't have negative width).
                You can mark a variable as <code>unsigned</code> to essentially
                double it's range (by getting rid of the negative side of values).
                <br><br>
                For instance, a normal <code>int</code> can store values from
                -2,147,483,648 to 2,147,483,647 -  but if you mark it as
                <code>unsigned</code>, then your range is 0 to 4,294,967,295.
            </p>
            
            <h3>Modern C++: <code>auto</code></h3>
            
            <p>
                In C++11<sub>(This means the 2011 update.)</sub> and later
                you can use the keyword <code>auto</code> as the "data type"
                in your variable declaration that includes an assignment statement.
                When you use <code>auto</code>, it uses the assigned value to automatically
                figure out the variable's data type, so you don't have to explicitly
                define it:
            </p>
        
<pre><code class="cpp">auto price = 9.99;      // it's a double!
auto price2 = 2.95f;    // it's a float!
auto player = '@';      // it's a char!
auto amount = 20;       // it's an int!
</code></pre>

        \begin{figure}[h]
            \centering
            \begin{subfigure}{.3\textwidth}
                \includegraphics[width=0.9\textwidth]{Basics/images/rachu}
            \end{subfigure}%
            \begin{subfigure}{.7\textwidth}
                
                Using C\#'s equivalent of \texttt{auto} (\texttt{var})
                is super common from what I've seen on the job.
                However, it still feels weird to me to use \texttt{auto}
                in C++. I tend to prefer to always be as explicit as
                possible in my programming to reduce any guesswork,
                even if it's something that takes half a second to figure out.
                
            \end{subfigure}
        \end{figure}
             
<!--
       
        
    \section{Basic operations on variables}
    
        Now that you have variables available in your program to play with,
        what can you even do with them?
        
        \subsection{Outputting variable values}
        
            Using the \texttt{cout} (console-out) statement, you can
            display text to the screen with string literals:
            
\begin{lstlisting}[style=code]
cout << "Hello, world!" << endl;
\end{lstlisting}

            But you can also display the values stored within variables,
            simply by using the variable's name:
        
\begin{lstlisting}[style=code]
cout << myUsername << endl;
\end{lstlisting}

            (We will cover more about input and output next part)

        \subsection{Inputting variable values}
        
            We can use the \texttt{cin} (console-in) statement to
            get the user to enter somethign on the keyboard and
            store that data into a variable:
        
\begin{lstlisting}[style=code]
cin >> favoriteColor;
\end{lstlisting}

        \subsection{Math operations}
        
            With variables with numeric data types (ints, floats, doubles)
            we can do arithmetic with the \texttt{+}, \texttt{-}, \texttt{*} and \texttt{/} operators.
            
\begin{lstlisting}[style=code]
cout << "Sum: " << num1 + num2 + num3 << endl;
\end{lstlisting}

            \paragraph{Operations:}
            \begin{center}
             \begin{tabular}{l l}
              \textbf{Symbol}   & \textbf{Description}  \\ \hline
              \texttt{+}        & Addition              \\
              \texttt{-}        & Subtraction           \\
              \texttt{*}        & Multiplication        \\
              \texttt{/}        & Division              \\
             \end{tabular}   
            \end{center}

            \paragraph{Make sure to put the result somewhere!}
            When you do a math operation and you want to use the result
            elseware in the program, make sure you're storing the result
            in a variable via an assignment statement!             
            If you just do this, nothing will happen:
\begin{lstlisting}[style=code]
totalCats + 1;
\end{lstlisting}

            You can use an assignment statement to store the result
            in a new variable...
\begin{lstlisting}[style=code]
newCatTotal = totalCats + 1;
\end{lstlisting}
            
            Or overwrite the variable you're working with...
\begin{lstlisting}[style=code]
totalCats = totalCats + 1;
\end{lstlisting}

            \begin{center}
             \includegraphics[height=3cm]{Basics/images/cats}   
            \end{center}
            
            \newpage
            \paragraph{Compound operations:}
            There are also shortcut operations you can use to quickly
            do some math and overwrite the original variable.
            This works with each of the arithmetic operations:
            
\begin{lstlisting}[style=code]
// Long way:
totalCats = totalCats + 5;

// Compound operation:
totalCats += 5;
\end{lstlisting}
        
        \subsection{String operations}
        
            Strings have some special operations you can do on them.
            You can also see a list of functions supported by
            strings here: ~\\
            \texttt{https://www.cplusplus.com/reference/string/string/}
            (We will cover more with strings in a later part).
            
            \paragraph{Concatenating strings:}
            You can use the \texttt{+} symbol to combine strings together.
            When used in this context, the + sign is called the \textbf{concatenation operator}.
        
\begin{lstlisting}[style=code]
string type = "pepperoni";
string food = "pizza";

// Creates the string "pepperoni pizza"
string order = type + " " + food;
\end{lstlisting}

            \paragraph{Letter-of-the-string:}
            You can also use the subscript operator \texttt{[ ]} (more on
            this when we cover arrays) to access a letter at some
            \textit{position} in the string. Note that in C++
            position starts at 0, not 1.

\begin{lstlisting}[style=code]
string food = "pizza";

char letter1 = food[0];  // Stores 'p'
char letter2 = food[1];  // Stores 'i'
char letter3 = food[2];  // Stores 'z'
char letter4 = food[3];  // Stores 'z'
char letter5 = food[4];  // Stores 'a'
\end{lstlisting}
    
    
    \newpage
    \section{Named constants}
    
        Whenever you find yourself using a \textbf{literal} value
        in your assignment statements, you may want to think about
        whether you should replace it with a \textbf{named constant}
        instead.
        
        A named constant looks like a variable when you declare it,
        but it also has the keyword \texttt{const} - meaning that
        the value can't change after its declaration.
    
            \begin{intro}{Named constant declaration format}
                \begin{enumerate}
                    \item   \texttt{const CONSTNAME = LITERAL;} ~\\
                            Stores the LITERAL value in CONSTNAME.
                \end{enumerate}
            \end{intro}
            
        Let's say you wrote a program and hard-coded the tax rate:
                
\begin{lstlisting}[style=code]
// Somewhere in the code...
checkoutPrice = cartTotal + ( cartTotal * 0.0948 );

// Somewhere else in the code...
cout << 0.0948 << " sales tax" << endl;
\end{lstlisting}

        Then the tax rate changes later on. You would have to go into
        your program and search for "0.0948" and update \textit{all those places!}
        
        Instead, it would have been easier to assign the tax rate ONCE
        to a named constant, and referred to that instead:
        
\begin{lstlisting}[style=code]
// Beginning of program somewhere...
const SALES_TAX = 0.0948;
    
// Somewhere in the code...
checkoutPrice = cartTotal + ( cartTotal * SALES_TAX );

// Somewhere else in the code...
cout << SALES_TAX << " sales tax" << endl;
\end{lstlisting}

        If you ever find yourself using the same literal multiple times
        in your program, then perhaps consider replacing it with a
        named constant.
        
        \paragraph{Named constant naming convention:}
        In C++, it is customary to give your named constants names in
        ALL CAPS, using underscores (\texttt{\_}) to separate words.
    
    %math operations
    %concatenating strings
    
    
    %compound operators *=
    %no commas or $ signs
    
    %why are floats
    % subscript with strings
    
    -->


          
            <hr><h2>Important vocabulary</h2>
            <ul>
                <li>Variable</li>
                <li>Data type</li>
                <li>Variable declaration</li>
                <li>Variable assignment</li>
                <li>Assignment statement</li>
                <li>Named constant</li>
            </ul>
          
          <div class="back-to-top">
            <a href="#top">Back to top</a> <br> <a href="../index.html">Back to index</a>
          </div>
          <hr>
          
          <div class="license">
            <p>Written by Rachel Wil Sha Singh</p>
            <p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>
          </div>
        </div></div> <!-- <div class="row"> -->
        <!-- === END BOOK CONTENTS === -->

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
