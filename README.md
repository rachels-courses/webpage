# CS 200: Concepts of Programming with C++

## Tentative schedule

| Week # | Week of | Unit(s)                                                              | Notes                                             |
| ------ | ------- | -------------------------------------------------------------------- | ------------------------------------------------- |
| 1      | Aug 21  | Unit 00: Welcome to the course!                                      | Aug 21 - First day of the fall semester           |
| 2      | Aug 28  | Unit 01: Exploring software<br/>Unit 02: main()                      | Aug 28 - Last day to drop and receive full refund |
| 3      | Sept 4  | Unit 03: Variables<br/>Unit 04: cin/cout                             |                                                   |
| 4      | Sept 11 | Unit 05: if/else if/else branching<br/>Unit 06: Switch branching     |                                                   |
| 5      | Sept 18 | Unit 07: while loops                                                 |                                                   |
| 6      | Sept 25 | RW CLASS BREAK WEEK                                                  | R.W.'s classes only                               |
| 7      | Oct 2   | Unit 08: Pointers and memory<br/>Unit 09: Functions                  |                                                   |
| 8      | Oct 9   | Unit 10: structs<br/>Unit 11: classes                                |                                                   |
| 9      | Oct 16  | Unit 12: for loops<br/>Unit 13: Storing data with arrays and vectors | Oct 16 - application deadline for fall graduation |
| 10     | Oct 23  | Unit 14: Strings<br/>Unit 15: ifstream and ofstream                  |                                                   |
| 11     | Oct 30  | RW CLASS BREAK WEEK                                                  | R.W.'s classes only                               |
| 12     | Nov 6   | Unit 16: Inheritance                                                 |                                                   |
| 13     | Nov 13  | Unit 17: Searching and Sorting<br/>Unit 18: Recursion basics         | Nov 15 - last day to withdraw with "W"            |
| 14     | Nov 20  | CATCH-UP WEEK                                                        | R.W.'s classes only                               |
| 15     | Nov 27  | ASSIGNMENT TURN-INS                                                  | ALL SUBMISSIONS DUE BY END OF WEEK!               |
| 16     | Dec 4   | FINALS WEEK                                                          | Dec 5 - 11: Final exams week                      |
| 17     | Dec 11  | POST SEMESTER                                                        | Dec 12: Grades entered online by 5 pm             |

## Course information

|                          |                                                     |
| ------------------------ | --------------------------------------------------- |
| Course                   | CS 200: Concepts of Programming with C++            |
| Semester                 | Fall 2023                                           |
| Section/CRN/Presentation | 377, CRN 81860, HyFlex<br/>Tuesdays, 6:00 - 8:50 pm |
| Instructor               | R.W. Singh (they/them)                              |
| Email                    | rsingh13@jccc.edu                                   |
| Office                   | RC 348H                                             |

## Quick links

* [Course discord](https://discord.gg/jj7U6HtVeh)

* [Book & Lectures](https://rachels-courses.gitlab.io/webpage/ref/review.html)

* [Syllabus](https://rachels-courses.gitlab.io/webpage/syllabus.html)

* [Quick Reference](https://rachels-courses.gitlab.io/webpage/ref/reference.html)
  
  * [C++ style guide](https://rachels-courses.gitlab.io/webpage/ref/style.html)
  
  * [C++ commands reference](https://rachels-courses.gitlab.io/webpage/ref/reference.html#quick-reference)

* [JCCC Course Catalog, CS 200](https://catalog.jccc.edu/coursedescriptions/cs/#CS_200)

* [All of R.W.'s Courses](https://rachels-courses.gitlab.io/webpage/)
