<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title> Function overloading (CS 235) - R.W.'s Courses </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="stylesheet" href="present-assets/style.css">
  <link rel="icon" type="image/png" href="present-assets/favicon.png">
  
  <style type="text/css">
    pre { border: solid 1px #000; padding: 5px; background: #fff; }
  </style>
  
  <!-- highlight.js -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>
<body>
  
  <!-- START OF contents -->
  <section id="contents">
    <section class="contents">
      <p><strong> Function overloading </strong></p>
      <ol>
        <li><a href="#slide1"> Function overloading </a></li>
        <li><a href="#slide2"> Unique signatures </a></li>
        <li><a href="#slide3"> Member functions </a></li>
        <li><a href="#slide4"> Overloading constructors </a></li>
        <li><a href="#slide5"> Shallow copy vs. deep copy </a></li>
        <hr>
        <li><a href="#additional">Example code and additional resources</a></li>
      </ol>
    </section>
  </section>
  <!-- END OF contents -->
  
  <!-- START OF presentation -->
  <section id="presentation">
    
    <!--
    <div class="accessibility-options">
      <section class="header"><a name="sec-welcome" href="#sec-welcome"><h1> Accessibility </h1></a></section>
      <section class="contents">
        <p>You can read through / play the audio for each slide at your own pace, or</p>
        <p><input type="button" id="auto-play" value="▶️ Auto-play slides like a video" onclick="AutoPlayBegin()"></p>
      </section>
    </div>
    -->

    <div class="slide">
      <section class="header"><a name="slide1" href="#slide1"><h1> Function overloading </h1></a></section>
      <section class="contents">
        <p>
			In C++ you can write multiple functions with the <strong>same name</strong>, 
			as long as it has a different <strong>parameter list</strong> so the compiler
			can tell the two functions apart. Doing this is called <strong>function overloading</strong>.
        </p>
        <pre><code class="cpp">int Sum( int a, int b )
{
    return a + b;
}

float Sum( float a, float b )
{
    return a + b;
}
        </code></pre>
        
        <p>
			Function overloading can be useful for scenarios where we might have different amounts of
			information to pass into the function at different times, or different versions of a function
			that handle different data types. For example:
        </p>
        <ul>
			<li> Setup function, given parameters A, B, C... - Maybe some info is available "here" but not "there". </li>
			<li> Display function, given different object types - Perhaps a "rectangle" is drawn differently from a "circle". </li>
        </ul>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide2" href="#slide2"><h1> Unique signatures </h1></a></section>
      <section class="contents">
        <p>
			You can write as many functions with shared names as you'd like, as long as each function
			has different parameters. This means:
        </p>
        <ul>
			<li> The functions have different amounts of parameters, or </li>
			<li> The data types of the parameters are different, or </li>
			<li> The parameters are in a different order (when mixing data types). </li>
        </ul>
        
        <p>
			Parameter variable names don't affect the "uniqueness" of a function signature,
			it has to be different data types, different order, or different amounts of parameters.
        </p>

        <pre><code class="cpp">void Setup( string log_file );
void Setup( string log_file, bool is_debug );

void Draw( Rectangle rect );
void Draw( Circle circle );
</code></pre>
      </section>
    </div>
    
    
    
    <div class="slide">
      <section class="header"><a name="slide3" href="#slide3"><h1> Member functions </h1></a></section>
      <section class="contents">
        <p>
			We can also overload member functions (aka <em>methods</em>) within a class. The same rules apply, just now the
			functions belong to a class.
        </p>
<pre><code class="cpp">// CLASS/FUNCTION DECLARATION
class Program
{
	public:
	void Setup( string log_file );
	void Setup( string log_file, bool is_debug );
};

// FUNCTION DEFINITIONS
void Program::Setup( string log_file )
{
	// ...
}

void Program::Setup( string log_file, bool is_debug )
{
	// ...
}

// WITHIN MAIN/OTHER FUNCTIONS:
Program prog;
prog.Setup( "Log.txt" );

Program debug_prog;
debug_prog.Setup( "DebugLog.txt", true );
</code></pre>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide4" href="#slide4"><h1> Overloading constructors </h1></a></section>
      <section class="contents">
        <p>
			A really handy use of overloading functions is to overload a class' <strong>constructors</strong>.
			There are three main types of constructors we may want for any given class:
        </p>
        <ol>
			<li> <strong>A Default Constructor</strong>, where member variables are given default values.
<pre><code class="cpp">File() {
	this-&gt;filename = "default.txt";
}</code></pre>
			</li>
			<li> <strong>A Parameterized Constructor</strong>, where initial data is provided to the class. 
<pre><code class="cpp">File( string name ) {
	this-&gt;filename = name;
}</code></pre>
			</li>
			<li> <strong>A Copy Constructor</strong>, where another object of this type is passed in, and we copy the data <em>from</em> that other object to <em>this</em> object.
<pre><code class="cpp">File( const File&amp; other ) {
	this-&gt;filename = other.filename;
}</code></pre>
			</li>
        </ol>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide5" href="#slide5"><h1> Shallow copy vs. deep copy </h1></a></section>
      <section class="contents">
        <p>
			When writing a copy constructor you will need to decide what <em>kind</em> of copy will occur...
        </p>
        
        <table style="font-size:0.8em;">
			<tr>
				<td style="width:30%;"><img src="images/cs235-unit12-overloadfunc_copy1.png" style="width:100%;"></td>
				<td style="width:70%;">
					A <strong>Shallow Copy</strong> is where values of variables are <em>copied over</em>.
					<br><br>
					This is usually fine for classes that don't work with pointers. However,
					if the class has a pointer as a member variable, keep in mind that the original
					and the copy will both <strong>point to the same memory address</strong>.
					<br><br>
					If you're not expecting this, this can create memory management errors down the road.
				</td>
			</tr>
			<tr>
				<td><img src="images/cs235-unit12-overloadfunc_copy2.png" style="width:100%;"></td>
				<td>
					A <strong>Deep Copy</strong> is where new memory is allocated for any of the
					pointer member variables. The original and the copy will point to different
					memory addresses, but you can copy the <em>value</em> so that both memory
					addresses have the same value.
					<br><br>
					This is usually safer, and usually more in-line with what someone would expect
					from a copy by default.
				</td>
			</tr>
        </table>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="additional" href="#additional"><h1> Example code and additional resources </h1></a></section>
      <section class="contents">
        <p><strong>Spring 2024 lecture:</strong></p>
        
        <ul>
            <!--<li><a href="">🎥 Class archive - Recursion (Spring 2024)</a></li>-->
            <li> WORK IN PROGRESS </li>
            <!-- <li><a href=""></a></li> -->
            <!-- 📺 Old video lecture -->
            <!-- 🎥 Class archive -->
            <!-- ⏲️ C++ in 60 seconds -->
        </ul>
      </section>
    </div>
    
  </section> 
  <!-- END OF presentation -->
  
  <script>
    function AutoPlayBegin()
    {
      PlaySlide( 1 );
    }
    
    function PlaySlide( page )
    {
      console.log( "Page:", page );
      location.replace( "#slide" + page );
      var time = ( document.getElementById( "slide" + page + "-audio" ).duration + 1 ) * 1000;
      console.log( "Duration:", time, "milliseconds" );
      document.getElementById( "slide" + page + "-audio" ).play();
      setTimeout( PlaySlide, time, page+1 );
    }
  </script>

</body>
