<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title> C++ Basics (CS 200) - R.W.'s Courses </title>
        <meta name="description" content="">
        <meta name="author" content="Rachel Singh">
        <link rel="stylesheet" href="present-assets/style.css">
        <link rel="icon" type="image/png" href="present-assets/favicon.png">


        <!-- highlight.js -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>

        <style type="text/css">
         pre { border: solid 1px #000; padding: 5px; background: #fff; }
         code { background: #FDFF91; padding: 5px; }
        </style>
    </head>
    <body>

        <!-- START OF contents -->
        <section id="contents">
            <section class="contents">
                <p><strong> C++ Basics </strong></p>
                <ol>
                    <li>Introduction to C++
                        <ul>
                            <li><a href="#slide1"> Files in a C++ project </a></li>
                            <li><a href="#slide2"> Contents of a C++ source file </a></li>
                            <li><a href="#slide3"> C++ statements </a></li>
                            <li><a href="#slide4"> Displaying text to the screen </a></li>
                            <li><a href="#slide5"> Program comments </a></li>
                            <li><a href="#slide6"> Syntax errors, logic errors </a></li>
                            <li><a href="#slide7"> Minimizing bugs and debugging time </a></li>
                        </ul>
                    </li>
                    <li>Variables and data types
                        <ul>
                            <li><a href="#slide8"> Variables </a></li>
                            <li><a href="#slide9"> Variable declaration and assignment  </a></li>
                            <li><a href="#slide10"> Variable arithmetic  </a></li>
                        </ul>
                    </li>
                    <li>Console input and output
                        <ul>
                            <li><a href="#slide11"> Outputting text and variables </a></li>
                            <li><a href="#slide12"> Getting input from the keyboard </a></li>
                            <li><a href="#slide13"> String input and cin.ignore() </a></li>
                        </ul>
                    </li>
                    <li> Memory addresses
                        <ol>
                            <li><a href="#slide14"> Memory addresses </a></li>
                            <li><a href="#slide15"> Address-of operator <code>&amp;</code> </a></li>
                        </ol>
                    </li>
                    <li> Pointer variables
                        <ol>
                            <li><a href="#slide16"> What is a pointer? </a></li>
                            <li><a href="#slide17"> Declaring a pointer </a></li>
                            <li><a href="#slide18"> Pointing to addresses </a></li>
                            <li><a href="#slide19"> Dereferencing pointers </a></li>
                            <li><a href="#slide20"> Why use pointers? </a></li>
                        </ol>
                    </li>
                    <li> Debugging pointers
                        <ol>
                            <li><a href="#slide21"> Dereferencing invalid memory </a></li>
                            <li><a href="#slide22"> What happens with ptr1 = ptr2? </a></li>
                        </ol>
                    </li>
                    <hr>
                    <li><a href="#additional"> Example code and additional resources </a></li>
                </ol>
            </section>
        </section>
        <!-- END OF contents -->

        <!-- START OF presentation -->
        <section id="presentation">

            <div class="slide">
                <section class="header"><a name="slide1" href="#slide1"><h1> Files in a C++ project </h1></a></section>
                <section class="contents">
                    <p>
                        At minimum, a C++ program requires one file - a .cpp source file.
                        Usually programs are bigger and contain multiple files, like .h header files as well as other .cpp source files.
                        The file that contains the program starting point is usually named main.cpp, but it doesn't have to have this name.
                        <br><br>
                        A C++ source file is technically just a plaintext file, which can be opened in a text editor like notepad,
                        though there are better code editors than notepad.
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide2" href="#slide2"><h1> Contents of a C++ source file </h1></a></section>
                <section class="contents">

                    <p>Every C++ program requires a main() function as its starting point. The most basic C++ program must have the following:</p>

                    <pre><code class="cpp">
int main()
{
    return 0;
}
                    </code></pre>


                    <p>For now you can take this code for granted but you will understand it more as the class continues. But, some basic info:</p>
                    <ul>
                        <li>The int main() is the function that starts our program.</li>
                        <li>The contents of the program must be between the opening curly brace { and the closing curly brace }. This region is known as main()'s code block. We will see more code blocks (with { }) later in the class.</li>
                        <li>The return 0; is where the program ends. 0 is returned for 0 errors.</li>
                        <li>Code we add will go after the { and before the return 0;.</li>
                    </ul>

                    <p>Additionally, C++ is case sensitive, so capitalization matters. Make sure you write main() in all lower case - Main() and MAIN() won't work!</p>

                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide3" href="#slide3"><h1> C++ statements </h1></a></section>
                <section class="contents">

                    <ul>
                        <li>C++ <strong>statements</strong> are single commands to be performed, and always end with a semicolon <code>;</code> </li>
                        <li>There are some commands we'll cover later that aren't just single commands, so they don't end with a semicolon. We will cover those later.</li>
                        <li>Example basic commands:
                            <ul>
                                <li>Declaring a variable: <pre><code>int total_cats;</code></pre></li>
                                <li>Assigning a value to a variable: <pre><code>total_cats = 10;</code></pre></li>
                                <li>Doing arithmetic: <pre><code>int result = number1 * number2;</code></pre></li>
                                <li>Displaying text to the screen: <pre><code>cout &lt;&lt; "Hello!";</code></pre></li>
                            </ul>
                        </li>
                    </ul>

                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide4" href="#slide4"><h1> Displaying text to the screen </h1></a></section>
                <section class="contents">
                    <p>In order to display text to the screen with our program, we need to include a library.A library is a set of pre-written code that can be used across multiple programs. C++ contains a standard library of pre-written code we can use. To gain access to the cout (console-output) statement, we need to add this at the top of our C++ program file:</p>
                    <pre><code>#include &lt;iostream&gt;</code></pre>
                    <p>After that, we can use the cout command to display text like this: <pre><code>std::cout &lt;&lt; "Hello!";</code></pre></p>

                    <p>The std:: prefix stands for "standard", as in the C++ standard library.
                        This command will display "Hello!" to the screen when the program is built and run.
                        <br>
                        You can also add empty lines to the program using the endl (end-line) command:</p>
                        <pre><code>std::cout &lt;&lt; std::endl;</code></pre>

                        <p>And these commands can be chained together:</p>
                        <pre><code>std::cout &lt;&lt; "Hello!" &lt;&lt; std::endl &lt;&lt; "World!" &lt;&lt; std::endl;</code></pre>
                        <p>The &lt;&lt; sign is known as the output stream operator.</p>

                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide5" href="#slide5"><h1> Program comments </h1></a></section>
                <section class="contents">
                    <p>We can leave <strong>comments</strong> in our code. Comments are meant for humans, and not the computer -
                        the compiler just ignores these lines. But, they can be handy to help clarify what's going on in your program.</p>

                    <p>A single line comment begins with two forward slashes:<br><br> <code>// Note: formula for area</code></p>

                    <p>Sometimes you need more text, so you can use a multi-line comment. These begin with <code>/*</code> and end with <code>*/</code> like this:</p>
                    <pre><code class="cpp">
/*
MY PROGRAM
This program does abcxyz stuff.
*/
                    </code></pre>

                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide6" href="#slide6"><h1> Syntax errors, logic errors </h1></a></section>
                <section class="contents">
                    <img src="https://rachels-courses.gitlab.io/webpage/web-assets/cuties/error-logic.png" style="float: right; margin: 10px;">

                    <p>We're going to be making a lot of <strong>errors</strong> during this semester and throughout our programming career!
                        As you keep practicing, you'll get better at the <em>diagnosis and fixing process</em>, but bugs will always occur.
                        I know they can be frustrating, but it's a normal part of development. Here are some tips...
                    </p>

                    <ul>
                        <li><strong>Syntax errors</strong> are errors in the language rules of your program code.
                            This include things like typos, misspellings, or just writing something that isn't valid C++ code, such as forgetting a ;
                            at the end of some instructions.<br><br>
                            Although they're annoying, syntax errors are probably the "best" type of error because the program won't build while they're present. Your program won't build if you have syntax errors.  </li>
                        <br><br>
                        <li> <strong>Logic errors</strong> are errors in the programmer's logic. This could be something like a wrong math formula, a bad condition on an if statement, or other things where the programmer thinks thing A is going to happen, but thing B happens instead.
                            <br><br>
                            These don't lead to build errors, so the program can still be run, but it may result in the program crashing, or invalid data being generated, or other problems.
                        </li>
                    </ul>

                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide7" href="#slide7"><h1> Minimizing bugs and debugging time </h1></a></section>
                <section class="contents">
                    <img src="https://rachels-courses.gitlab.io/webpage/web-assets/cuties/coding-strategy.png" style="float: right; margin: 10px;">

                    <p>We're going to be making a lot of <strong>errors</strong> during this semester and throughout our programming career!
                        As you keep practicing, you'll get better at the <em>diagnosis and fixing process</em>, but bugs will always occur.
                        I know they can be frustrating, but it's a normal part of development. Here are some tips...
                    </p>

                    <ul>
                        <li>Write a few lines of code and build after every few lines - this will help you detect syntax errors early.</li>
                        <br>
                        <li>DON'T write the "entire program" before ever doing a build or a test - chances are you've written in some syntax errors, and now you'll have a long list of errors to sift through!</li>
                        <br>
                        <li>If you aren't sure where an error is coming from, try commenting out large chunks of your program until it's building again. Then, you can un-comment-out small bits at a time to figure out where the problem is.</li>
                    </ul>

                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide8" href="#slide8"><h1> Variables </h1></a></section>
                <section class="contents">
                    <img src="images/cs200-unit03-cpp_basics_variables.png" style="float: right; margin: 10px;">

                    <ul>
                        <li> <strong>Variables</strong> are a place where we can store data under some "name". </li>
                        <li> These variables may be used to store numbers that we can do math with, or perhaps store records
                            for a user, or many other things -- any software you use in your daily life has variables built in. </li>
                        <li> <strong>Data types</strong> include integers (whole numbers), floating point numbers (numbers with decimals),
                            strings (text), booleans (true/false), characters (single letters/symbols), and we can even eventually make
                            our own data types.</li>
                    </ul>
                    <table style="width:100%;">
                        <tr><th>Data Type</th><th>Example values</th></tr>
                        <tr><td>int</td><td>-5, 1, 45</td></tr>
                        <tr><td>float</td><td> 98.5, 7.75 </td></tr>
                        <tr><td>string</td><td> "Username", "HI!" </td></tr>
                        <tr><td>char</td><td> '$', 'x' </td></tr>
                        <tr><td>bool</td><td> true, false </td></tr>
                    </table>

                    <p>
                        Variable names can contain letters, numbers, and underscores - but NO SPACES!
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide9" href="#slide9"><h1> Variable declaration and assignment </h1></a></section>
                <section class="contents">
                    <img src="images/cs200-unit03-cpp_basics_variables.png" style="float: right; margin: 10px;">

                    <p><strong>Variable declaration:</strong>
                        In our C++ programs, we need to tell our program about a variable <em>before</em>
                        we start using it. This type of instruction is called a <strong>variable declaration</strong>.
                        To do this, we specify the variable's <strong>DATA TYPE</strong> and give it a <strong>NAME</strong>.
                    </p>

                    <pre><code>string username;
int total_followers;</code></pre>

                    <p>
                        Programs flow from the top to the bottom (until we get to functions :)
                        so before you use any variables, they must be <em>declared above</em>.
                    </p><br>

                    <p><strong>Variable assignment:</strong>
                        You can assign values to variables in C++ using the assignment operator, <code>=</code>,
                        like this:
                    </p>
                    <pre><code>username = "MooseInvader";
total_followers = 12345;</code></pre>

                    <p>You can also assign a value to a variable during its declaration. This is known as <strong>initializing</strong> the variable:</p>
                    <pre><code>string username = "Texpert";</code></pre>
                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide10" href="#slide10"><h1> Variable arithmetic </h1></a></section>
                <section class="contents">
                    <p>With numeric variables you may need to do arithmetic with them. The basic arithmetic operators are:</p>
                    <table style="width:100%;">
                        <tr><th>Operation</th><th>Operator</th><th>Example code</th></tr>
                        <tr><td>Add</td><td><code>+</code></td><td><code>total_posts = video_post_count + text_post_count;</code></td></tr>
                        <tr><td>Subtract</td><td><code>-</code></td><td><code>total_followers = total_followers - 1;</code></td></tr>
                        <tr><td>Multiply</td><td><code>*</code></td><td><code>area = width * length;</code></td></tr>
                        <tr><td>Divide</td><td><code>/</code></td><td><code>price_per_pizza = 9.99 / 3;</code></td></tr>
                    </table>

                    <p>
                        When writing a command statement with a math expression, the <strong>LHS (left-hand-side)</strong>
                        of the equal sign is WHERE TO STORE THE RESULT. The <strong>RHS (right-hand-side)</strong>
                        of the equal sign is THE MATH EXPRESSION TO BE SOLVED.
                    </p>

                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide11" href="#slide11"><h1> Outputting text and variables </h1></a></section>
                <section class="contents">
                    <p>
                        We can use the <strong><code>cout</code></strong> command (pronounced "C-Out" for Console Output)
                        to display text to the screen. We can display string literals <code>"like this"</code> -
                        which are hard-coded text we put in - or we can display variable values.
                    </p>
                    <p>Note that to use output and input commands, we need to add <code>#include &lt;iostream&gt;</code> at the top of our code file.
                        This is a <strong>library</strong> of pre-written code we can utilize.</p>

                    <pre><code class="cpp">
#include &lt;iostream&gt;
using namespace std;

int main()
{
  // endl means "end line", starts the next text on a new line.
  cout &lt;&lt; "Hello," &lt;&lt; endl;
  cout &lt;&lt; "world!" &lt;&lt; endl;

  return 0;
}
                    </code></pre>

                    <p>
                        If we want to display a variable's <strong>value</strong>,
                        we put the variable's name in the cout command:
                    </p>
                    <pre><code>cout &lt;&lt; total_cats &lt;&lt; endl;</code></pre>

                    <p>
                        And when displaying variable values, it's best to add a string-literal label
                        so that the user knows what the data they're looking at means.
                    </p>
                    <pre><code class="cpp">cout &lt;&lt; "Total cats I have: " &lt;&lt; total_cats &lt;&lt; endl;</code></pre>
                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide12" href="#slide12"><h1> Getting input from the keyboard </h1></a></section>
                <section class="contents">
                    <p>
                        Often we want our programs to be <strong>interactive</strong>, so that the user can interface
                        with it and our program can respond to the user's input. The first step to this is getting
                        input from the keyboard and storing that input in a variable. We do this with the
                        <code>cin</code> ("C-in", Console Input) command:
                    </p>

                    <pre><code class="cpp">
#include &lt;iostream&gt;
using namespace std;

int main()
{
  cout &lt;&lt; "Enter your favorite number: " &lt;&lt; endl;
  int favorite_number;
  cin &gt;&gt; favorite_number;     // Getting input and storing in favorite_number variable
  cout &lt;&lt; "Your favorite number is " &lt;&lt; favorite_number &lt;&lt; endl;

  return 0;
}
                    </code></pre>

                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide13" href="#slide13"><h1> String input and cin.ignore() </h1></a></section>
                <section class="contents">
                    <p>
                        If we want to get <strong>string</strong> text, we <em>can</em> use <code>cin &gt;&gt;</code>,
                        but that won't capture any spaces. If you want to get information like a name, address, etc.,
                        you will probably want to use the <code>getline</code> function instead:
                    </p>

                    <pre><code class="cpp">#include &lt;iostream&gt;
using namespace std;

int main()
{
  cout &lt;&lt; "Enter your username: ";
  string username;
  getline( cin, username );   // Get a full line of text

  return 0;
}
                    </code></pre>

                    <p>
                        However, if you inter-mix <code>cin &gt;&gt; ...</code> and <code>getline( cin, ... )</code>,
                        then we will run into some buffer issues. If your program is skipping an input, that means you
                        need to add a <code>cin.ignore();</code> to your program. This ONLY needs to happen when going FROM
                        <code>cin &gt;&gt; ...</code> TO <code>getline( cin, ... )</code>:
                    </p>
                    <pre><code class="cpp">int pin;
string username;

cout &lt;&lt; "Enter your PIN: ";
cin &gt;&gt; pin;

cin.ignore();   // Flush the input buffer

cout &lt;&lt; "Enter your username: ";
getline( cin, username );   // Get a full line of text
                    </code></pre>

                </section>
            </div>



            <div class="slide">
                <section class="header"><a name="slide14" href="#slide14"><h1> Memory addresses </h1></a></section>
                <section class="contents">
                    <p>
                        When we declare a variable, what we're actually doing is telling the computer to set aside some memory (in RAM) to hold some information. Depending on what data type we declare, a different amount of memory will need to be reserved for that variable. 
                    </p>
                    <table>
                        <tr>
                            <th> Data type </th> <td> boolean </td> <td> character </td> <td> integer </td> <td> float </td> <td> double </td>
                        </tr>
                        <tr>
                            <th> Size </th> <td> 1 byte </td> <td> 1 byte </td> <td> 4 bytes </td> <td> 4 bytes </td> <td> 8 bytes </td>
                        </tr>
                    </table>

                    <p>
                        (A <strong>bit</strong> is one digit: 0 or 1. A <strong>byte</strong> is 8 bits.)
                    </p>
                    <hr>
                    <p>
                        Any time we declare a variable, it is given a space in <strong>memory</strong> to store its data.
                        You can think of this as being spots in RAM, but the Operating System actually gives each program
                        a "virtual" memory space and is an intermediate between system resources (RAM) and the software/programs.
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide15" href="#slide15"><h1> Address-of operator &amp; </h1></a></section>
                <section class="contents">
                    <p>
                        We can use the <strong>address-of operator <code>&amp;</code></strong> to see the memory address of any variable.
                    </p>

                    <p> Example: </p>
                    <pre><code class="cpp"> cout &lt;&lt; &number1 &lt;&lt; "\t" &lt;&lt; &number2 &lt;&lt; endl;</code></pre>
                    <p> Program output: </p>
                    <pre class="terminal">0x7ffd3a24cc70	0x7ffd3a24cc74</pre>

                    <p>
                        Each time the program runs, different memory addresses are shown. When a program ends, the variables we've declared
                        are destroyed and that memory is freed up. When a program starts, then the program variables take whatever memory spaces
                        are available.
                    </p>

                    <p>
                        (Note: We've used &amp; before for pass-by-reference parameters. When it's after the data type in a variable declaration it's pass-by-reference. When it's before a variable name (not in a declaration), it's address-of.)
                    </p>
                </section>
            </div>



            <div class="slide">
                <section class="header"><a name="slide16" href="#slide16"><h1> What is a pointer? </h1></a></section>
                <section class="contents">
                    <p style="text-align:center;">
                        <img src="images/cs200-unit09-pointers_pointer.png">
                    </p>
                    <p>
                        A <strong>pointer</strong> is special a type of variable. Most variables store data like ints,
                        chars, floats, etc., but a pointer variable stores <em>memory addresses</em> as its value.
                        We can grab the address of any other variable and <em>point to it.</em>
                    </p>
                    <p>
                        A pointer declaration looks like a normal variable declaration, except with an extra symbol *.
                        We specify a DATA TYPE and a VARIABLE NAME during declaration.
                    </p>
                    <p>
                        Where TYPE should be the type of data we're pointing to - such as an integer, float, etc.
                        We tell it a type because each data type has a different amount of space it takes up in memory.
                        <br><br>
                        We can name the pointer variable anything along with our normal variable naming rules,
                        but it is common to prefix your pointer variable name with "ptr" just to make it obvious that
                        it is a pointer.
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide17" href="#slide17"><h1> Declaring a pointer </h1></a></section>
                <section class="contents">
                    <p>
                    </p>
                    <p>
                        A pointer declaration takes this form: <code class="cpp"> TYPE* NAME; </code>
                    </p>
                    <pre><code class="cpp"> int* ptrNumber = nullptr;
float* ptrPrice = nullptr; </code></pre>
                    <p>

                        It is best to assign all pointers to <code>nullptr</code> when they're not in use.
                        <code>nullptr</code> is an obviously "invalid memory address", and we can use this for
                        error checking.
                        <br><br>
                        If we don't initialize our pointer to <code>nullptr</code>, then it starts off with "garbage data".
                        We can't tell if garbage data is a valid memory address or not.
                    </p>
                    <p>
                        After a pointer is declared, we can point it to different memory addresses at any time in our program -
                        we're just assigning it a new value.
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide18" href="#slide18"><h1> Pointing to addresses </h1></a></section>
                <section class="contents">
                    <p>
                        To assign an address to a pointer variable we use the <strong>address-of operator <code>&amp;</code></strong>
                        to access a variable's address.
                    </p>
                    <pre><code class="cpp">int * ptr = nullptr; // Declare pointer
int var = 10; // Declare normal variable
ptr = &var; // Store var's address in the ptr variable
                    </code></pre>
                    <p>
                        If we use <code>cout</code> to display <code>ptr</code>, it will give us the same thing as if
                        we used <code>cout</code> on <code>&amp;var</code>...
                    </p>
                    <pre><code class="cpp">cout &lt;&lt; "var address: " &lt;&lt; &var &lt;&lt; endl;
cout &lt;&lt; "ptr value:   " &lt;&lt; ptr &lt;&lt; endl;</code></pre>
                    <pre class="terminal">var address: 0x7ffc3d6028b0
ptr value:   0x7ffc3d6028b0</pre>
                    <p style="text-align:center;"><img src="images/cs200-unit09-pointers_ptrvar.png"></p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide19" href="#slide19"><h1> Dereferencing pointers </h1></a></section>
                <section class="contents">
                    <p>
                        When a pointer is pointing to some address, we often want to access the <strong>value stored at that address</strong> -
                        either to write it out somewhere or to overwrite the value. We can do this by <strong>dereferencing our pointer</strong>.
                        <br><br>
                        Dereferencing a pointer takes this form: <code>*PTR</code>
                    </p>
                    <pre><code class="cpp">int * ptr;    // Declare pointer
int var = 10; // Declare normal variable

ptr = &var;   // Store var's address in the ptr variable

cout &lt;&lt; "var value:        " &lt;&lt; var &lt;&lt; endl;  // Original variable
cout &lt;&lt; "ptr value:        " &lt;&lt; ptr &lt;&lt; endl;  // Pointer's value is an address
cout &lt;&lt; "ptr dereferenced: " &lt;&lt; *ptr &lt;&lt; endl; // Dereferencing the pointer </code></pre>

                    <pre class="terminal">
var value:        10
ptr value:        0x7ffd21de775c
ptr dereferenced: 10
                    </pre>
                </section>
            </div>

            <div class="slide">
                <section class="header"><a name="slide20" href="#slide20"><h1> Why use pointers? </h1></a></section>
                <section class="contents">
                    <p>
                        We're just starting to cover pointers and it can be hard to come up with "good reasons" for them this early on.
                        Here are a few reasons we use pointers:
                    </p>
                    <ul>
                        <li> Point to the current item being modified instead of writing duplicate code for modifying each item. </li>
                        <li> Can have pointer parameters and pass pointers; similar use as pass-by-reference, just different notation.
                            <pre><code class="cpp">void Quadratic_Reference( float a, float b, float c, float & x1, float & x2 ); // Pass by ref version
void Quadratic_Pointer  ( float a, float b, float c, float * x1, float * x2 ); // Pass by pointer version</code></pre>
                        </li>
                        <li> Dynamically allocate memory for variables and arrays...
                            <ul>
                                <li> Dynamically allocated memory for variables: Build link-based data structures (Linked Lists, Binary Search Trees) - CS 250 topic :)</li>
                                <li> Dynamically allocated memory for arrays: Can create "resizable" arrays; don't need to know array size at compile time.
                            </ul>
                                </li>
                    </ul>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide21" href="#slide21"><h1> Debugging - Dereferencing invalid memory </h1></a></section>
                <section class="contents">
                    <p>
                        Pointers open us up to ways to accidentally cause our programs to crash. In particular, we need to look out for
                        <strong>dereferencing invalid addresses</strong>.
                    </p>
                    <p>Remember that if we declare an int without setting a value, it will start with "garbage" as its value:</p>

                    <pre><code class="cpp">int number;
cout &lt;&lt; number &lt;&lt; endl;</code></pre>
                    <pre class="terminal">32741</pre>

                    <p>
                        The same issue happens with a pointer. If we don't initialize it, it will get "garbage", and when we
                        display it it will look like a memory address, but it's an invalid one!
                    </p>
                    <pre><code class="cpp">int * ptr;
cout &lt;&lt; ptr &lt;&lt; endl;
                    </code></pre>
                    <pre class="terminal">0x7f7efe1fc934</pre>
                    <p>
                        If we dereference this invalid memory address, the program will crash or give some kind of
                        <strong>undefined behavior</strong>. This is why we initialize our pointers to <code>nullptr</code>!
                    </p>
                </section>
            </div>


            <div class="slide">
                <section class="header"><a name="slide22" href="#slide22"><h1> ptr1 = ptr2? </h1></a></section>
                <section class="contents">
                    <p style="text-align:center;"><img src="images/cs200-unit09-pointers_samepoint.png"></p>
                    <p>
                        If you set one pointer equal to another pointer, it will copy the memory address over, so both pointers
                        will be pointing to the same location.
                        Sometimes this is what you want, but it can also cause errors if you're not expecting it!
                    </p>
                </section>
            </div>














            <div class="slide">
                <section class="header"><a name="additional" href="#additional"><h1> Example code and additional resources </h1></a></section>
                <section class="contents">
                    <p><strong>Example code:</strong></p>
                    <ol>
                        <li><a href="https://gitlab.com/rachels-courses/example-code/-/blob/main/C++/CS200_01_Variables_cout_cin/superbasics_v1.cpp?ref_type=heads">Super basics v1</a></li>
                        <li><a href="https://gitlab.com/rachels-courses/example-code/-/blob/main/C++/CS200_01_Variables_cout_cin/superbasics_v2.cpp?ref_type=heads">Super basics v2</a></li>
                        <li><a href="https://gitlab.com/rachels-courses/example-code/-/blob/main/C++/CS200_01_Variables_cout_cin/Mad%20lib%20story/mad_lib_story.cpp?ref_type=heads">Mad lib</a></li>
                        <li><a href="https://gitlab.com/rachels-courses/example-code/-/blob/main/C++/CS200_01_Variables_cout_cin/Product%20prices/Variables%20Input%20and%20Output/main.cpp?ref_type=heads">Item price, name, quantity</a></li>
                    </ol>

                    <hr>
                    <p><strong>Archived videos and class lectures:</strong></p>
                    <ul>
                        <li><a href="https://www.youtube.com/shorts/ScaOW_f2Ig0">⏲️ C++ in 60 seconds - main() is the starting point of programs</a></li>
                        <li><a href="https://www.youtube.com/watch?v=9nNGo-rFPdM">⏲️ C++ in 60 seconds - cout lets you output text to the console</a></li>
                        <li><a href="https://www.youtube.com/shorts/zrVPuFfzhzw">⏲️ C++ in 60 seconds - common data types we use for variables</a></li>
                        <li><a href="https://www.youtube.com/shorts/WpmPE_-930Q">⏲️ C++ in 60 seconds - variable declarations and assignments</a></li>
                        <li><a href="https://www.tiktok.com/@rwsskc/video/7146577130120318250">⏲️ C++ in 60 seconds - pointerss</a></li>
                        <li><a href="http://lectures.moosader.com/cs200/02-CPP-Basics.mp4">📺 Old video lecture - C++ Basics (11:24)</a></li>
                        <li><a href="http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-26_cs200_lecture.mp4">🎥 Archive - Example code - Program basics (Spring 2021)</a></li>
                    </ul>
                </section>
            </div>



        </section>
        <!-- END OF presentation -->

    </body>
