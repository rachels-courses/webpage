<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title> Smart array structures (CS 250) - R.W.'s Courses </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="stylesheet" href="present-assets/style.css">
  <link rel="icon" type="image/png" href="present-assets/favicon.png">
  
  <style type="text/css">
    pre { border: solid 1px #000; padding: 5px; background: #fff; }
  </style>
  
  <!-- highlight.js -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>
<body>
  
  <!-- START OF contents -->
  <section id="contents">
    <section class="contents">
      <p><strong> Smart array structures </strong></p>
      <ol>
		  <li> Smart fixed-length array 
			  <ol>
				<li><a href="#slide1"> Smart fixed-array class </a></li>
				<li><a href="#slide2"> Initialization </a></li>
				<li><a href="#slide3"> IsEmpty, IsFull </a></li>
				<li><a href="#slide4"> Size, Clear </a></li>
				<li><a href="#slide5"> Search </a></li>
				<li><a href="#slide6"> ShiftRight </a></li>
				<li><a href="#slide7"> ShiftLeft </a></li>
				<li><a href="#slide8"> PushBack </a></li>
				<li><a href="#slide9"> PopBack </a></li>
				<li><a href="#slide10"> GetBack </a></li>
				<li><a href="#slide11"> PushFront </a></li>
				<li><a href="#slide12"> PopFront </a></li>
				<li><a href="#slide13"> GetFront </a></li>
				<li><a href="#slide14"> PushAt </a></li>
				<li><a href="#slide15"> PopAt </a></li>
				<li><a href="#slide16"> GetAt </a></li>
			  </ol>
		  </li>
		  
		  <li> Smart dynamic array 
			  <ol>
				<li><a href="#slide17"> Smart dynamic array class </a></li>
				<li><a href="#slide18"> Constructor and Allocate memory </a></li>
				<li><a href="#slide19"> Destructor and Deallocate memory </a></li>
				<li><a href="#slide20"> Resize </a></li>
				<li><a href="#slide21"> Changes from smart fixed array </a></li>
			  </ol>
		  </li>
        <hr>
        <li><a href="#additional">Example code and additional resources</a></li>
      </ol>
    </section>
  </section>
  <!-- END OF contents -->
  
  <!-- START OF presentation -->
  <section id="presentation">
    
    <!--
    <div class="accessibility-options">
      <section class="header"><a name="sec-welcome" href="#sec-welcome"><h1> Accessibility </h1></a></section>
      <section class="contents">
        <p>You can read through / play the audio for each slide at your own pace, or</p>
        <p><input type="button" id="auto-play" value="▶️ Auto-play slides like a video" onclick="AutoPlayBegin()"></p>
      </section>
    </div>
    -->

    <div class="slide">
      <section class="header"><a name="slide1" href="#slide1"><h1> Smart fixed-array class </h1></a></section>
      <section class="contents">
        <p>
			For this introductory data structure we are wrapping a basic fixed-length array within a class
			and creating helper member variables and functions to simplify its usage so that the user / other
			programmers don't have to worry about tracking the array size, item count, etc.
        </p>
        <p>
			The <code>SmartFixedArray</code> class contains the following member variables:
        </p>
        <table>
			<tr><th> Member variable </th><th> Description </th></tr>
			<tr>
				<td> <pre><code class="cpp">T m_array[100]</code></pre> </td>
				<td> The array structure itself, hard-coded at a size of 100. </td>
			</tr>
			<tr>
				<td> <pre><code class="cpp">const size_t ARRAY_SIZE</code></pre> </td>
				<td> Will store the array size 100. Storing this in a named const helps with code readability. </td>
			</tr>
			<tr>
				<td> <pre><code class="cpp">size_t m_itemCount</code></pre> </td>
				<td> Stores how many items are <em>actually</em> stored in the array, starting at 0. </td>
			</tr>
        </table>
        
        <p>Additionally, our smart array <em>will not allow gaps between elements.</em></p>
        <p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_contiguous.png">
        </p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide2" href="#slide2"><h1> Initialization </h1></a></section>
      <section class="contents">
        <p>
			To start off with, the <code>ARRAY_SIZE</code> const needs to be initialized via the constructor like this:
        </p>
<pre><code class="cpp">template &lt;typename T&gt;
SmartFixedArray<T>::SmartFixedArray()
    : ARRAY_SIZE( 100 )</code></pre>
        
        <p>
			Within the constructor, we should also initialize <code>m_itemCount</code> to 0:
        </p>
        <pre><code class="cpp">template &lt;typename T&gt;
SmartFixedArray<T>::SmartFixedArray()
    : ARRAY_SIZE( 100 )
{
    Clear();
}</code></pre>

		<p>
			The array itself doesn't change size, it will always have 100 "blocks",
			but we signify how many items have actually been <em>added</em> with the
			<code>m_itemCount</code> variable.
		</p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_initialized.png">
		</p>
      </section>
    </div>
    
    
    
    <div class="slide">
      <section class="header"><a name="slide3" href="#slide3"><h1> IsEmpty, IsFull </h1></a></section>
      <section class="contents">
		  
        <p>
			The array is considered empty if the item count is 0.
        </p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_initialized.png">
		</p>
		<br>
		<p>
			The array is considered full if the item count matches the array size.
		</p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_full.png">
		</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide4" href="#slide4"><h1> Size, Clear </h1></a></section>
      <section class="contents">
        <p>
			The "Size" is the amount of items currently stored in the array, so the item count.
        </p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_size.png">
		</p>
		<br>
        <p>
			To "Clear" the structure, we can lazy-delete... We just mark the item count to 0,
			and next time an item is added to the array, the old data will be deleted.
        </p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_clear.png">
		</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide5" href="#slide5"><h1> Search </h1></a></section>
      <section class="contents">
        <p>
			The search functionality is just a basic <strong>linear search</strong>.
			We begin at the start of the array and see if what we're looking for is at that position.
			If it is, we return the index. 
        </p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_search1.png">
		</p>
		<p>Otherwise, if the item isn't found (the loop completes
			and nothing is returned), we might throw an exception or return a -1 to symbolize "none".</p>
		<p style="text-align:center;">
			<img src="images/cs250-unit13-smartarrays_search2.png">
		</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide6" href="#slide6"><h1> ShiftRight </h1></a></section>
      <section class="contents">
        <p>
			The ShiftRight function is a helper we use when we want to insert new data in the middle of the array somewhere.
			This function takes an index, and shifts everything to the right over by 1, preparing an open space for new data to go in.
        </p>
        <p>
			Note that this is just a helper function so it doesn't adjust the item count - the main insert function will take care of that.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_shiftright.png">
		</p>
		<p><strong>Error check:</strong> If the array is full, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide7" href="#slide7"><h1> ShiftLeft </h1></a></section>
      <section class="contents">
        <p>
			ShiftLeft is another helper function, which is used for when we're removing data from the middle of the array.
			When that happens, a "gap" of unused data would be created, so instead we shift everything at that index to the 
			left by 1 to fill in the gap.
        </p>
        <p>
			Again, this is a helper function, so it won't adjust the item count. The remove function will take care of this.
        </p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide8" href="#slide8"><h1> PushBack </h1></a></section>
      <section class="contents">
        <p>
			The PushBack function will take in some data to be stored in the array.
			It will add that item into the array at the next available index, filling from left-to-right:
			0, then 1, then 2, and so on.
			<br><br>
			Additionally, whatever the current value of item count is, also happens to be the index
			where we will insert the new item.
			<br><br>
			Each time an item is added, the item count goes up by 1.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_pushback.png">
		</p>
		<p><strong>Error check:</strong> If the array is full, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide9" href="#slide9"><h1> PopBack </h1></a></section>
      <section class="contents">
        <p>
			PopBack removes the last item in the array, but we lazy delete it - just subtract 1 from the item count.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_popback.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide10" href="#slide10"><h1> GetBack </h1></a></section>
      <section class="contents">
        <p>
			Get back retrieves the item at the end of the array, at position <em>item count - 1</em>
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_getback.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide11" href="#slide11"><h1> PushFront </h1></a></section>
      <section class="contents">
        <p>
			PushFront adds a new item at the start of the list, at position 0.
			<br><br>
			We need to first call ShiftRight on position 0, and then we can add the new item at index 0.
			<br><br>
			Make sure to add 1 to the item count afterwards.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_pushfront.png">
		</p>
		<p><strong>Error check:</strong> If the array is full, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide12" href="#slide12"><h1> PopFront </h1></a></section>
      <section class="contents">
        <p>
			PopFront removes the item at position 0 in the array.
			<br><br>
			Afterwards, ShiftLeft needs to be called on index 0 to clear the gap.
			<br><br>
			Make sure to subtract 1 from the item count afterwards.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_popfront.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide13" href="#slide13"><h1> GetFront </h1></a></section>
      <section class="contents">
        <p>
			GetFront returns the item at index 0.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_getfront.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide14" href="#slide14"><h1> PushAt </h1></a></section>
      <section class="contents">
        <p>
			PushAt takes in an index as well as the new data and it will insert the new data at that position.
			<br><br>
			Before we can do that, we need to call ShiftRight at that index to make room for the new data.
			<br><br>
			Remember to add 1 to item count afterwards.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_pushat.png">
		</p>
		<p><strong>Error check:</strong> If the array is full, then this function fails.</p>
		<p><strong>Error check:</strong> If the index is invalid, then this function fails.
			The index is invalid if it's less than 0 or greater than the item count.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide15" href="#slide15"><h1> PopAt </h1></a></section>
      <section class="contents">
        <p>
			PopAt removes an item at some index. We lazy delete it by covering it up.
			<br><br>
			Call ShiftLeft at that index to shift all items over, replacing the old data there.
			<br><br>
			Remember to subtract 1 from item count afterwards.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_popat.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
		<p><strong>Error check:</strong> If the index is invalid, then this function fails.
			The index is invalid if it's less than 0 or greater than or equal to the item count.</p>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="slide16" href="#slide16"><h1> GetAt </h1></a></section>
      <section class="contents">
        <p>
			Returns whatever item is at the index given.
        </p>
		<p style="text-align:center;">
		<img src="images/cs250-unit13-smartarrays_getat.png">
		</p>
		<p><strong>Error check:</strong> If the array is empty, then this function fails.</p>
		<p><strong>Error check:</strong> If the index is invalid, then this function fails.
			The index is invalid if it's less than 0 or greater than or equal to the item count.</p>
      </section>
    </div>
    
    <div class="slide">
      <section class="header"><a name="slide17" href="#slide17"><h1> Smart dynamic array class </h1></a></section>
      <section class="contents">
        <p>
			The SmartDynamicArray class has a lot of shared functionality with the SmartFixedArray, except:
        </p>
        <ol>
			<li> If the array is full, we can <strong>resize</strong> it and then add more items to it. </li>
			<li> We need to deal with memory allocation. </li>
			<li> We need to write a resize function. </li>
        </ol>
        <br>
        
        <p>The member variables of the SmartDynamicArray change slightly:</p>
        <table>
			<tr><th> Member variable </th><th> Description </th></tr>
			<tr>
				<td> <pre><code class="cpp">T* m_array</code></pre> </td>
				<td> A pointer we will use to allocate space for the array. </td>
			</tr>
			<tr>
				<td> <pre><code class="cpp">size_t m_arraySize</code></pre> </td>
				<td> Will store the array size. </td>
			</tr>
			<tr>
				<td> <pre><code class="cpp">size_t m_itemCount</code></pre> </td>
				<td> Stores how many items are <em>actually</em> stored in the array, starting at 0. </td>
			</tr>
        </table>
      </section>
    </div>
    
    <div class="slide">
      <section class="header"><a name="slide18" href="#slide18"><h1> Constructor and Allocate memory </h1></a></section>
      <section class="contents">
        <p>
			For the <strong>constructor</strong>, we need to make sure to initialize <code>m_array</code> to <code>nullptr</code>.
        </p>
        <br>
        <p>For <strong>AllocateMemory</strong>, we take in some size. Using the <code>m_array</code> pointer,
        we create a new array of that size.
        <br><br>
        Also set item count to 0 and the array size variable to the size given.
        <br><br>
        <strong>Error check:</strong> If the array pointer is pointing to nullptr, then this function fails.
        </p>
      </section>
    </div>
    
    <div class="slide">
      <section class="header"><a name="slide19" href="#slide19"><h1> Destructor and Deallocate memory </h1></a></section>
      <section class="contents">
        <p>
			For the <strong>destructor</strong>, we should call DeallocateMemory to clean everything up.
        </p>
        <br>
        <p>
			For <strong>DeallocateMemory</strong>, if the array pointer is NOT pointing to nullptr that means
			it's assigned an address right now - in this case, delete the memory at the array address
			and then reset the array pointer to nullptr.
			Additionally set item count and array size to 0.
        </p>
      </section>
    </div>
    
    <div class="slide">
      <section class="header"><a name="slide20" href="#slide20"><h1> Resize </h1></a></section>
      <section class="contents">
        <p>
			This function is called with a larger size provided - the new size we want the array to be.
			The following are steps to "resize" a dynamic array:
        </p>
        <ol>
			<li> Allocate memory for a new array with the new size. </li>
			<li> Use a for loop over all the items, copy from the old array to the new array. </li>
			<li> Delete the array at the old array address. </li>
			<li> Update the "old" array to point to the new array address. </li>
			<li> Update the array size to be the bigger size. </li>
        </ol>
        <p><strong>Error check:</strong> if the new size is less than the current array size, this function fails.</p>
      </section>
    </div>
    
    <div class="slide">
      <section class="header"><a name="slide21" href="#slide21"><h1> Changes from smart fixed array </h1></a></section>
      <section class="contents">
        <p>
			Other things to change:
        </p>
        <ul>
			<li> All Push functions and the ResizeRight function need to be updated so that, if they're full, just call Resize and then resume adding the data afterwards (no longer a failure case). </li>
			<li> May need to add nullptr checks for the array pointer. </li>
        </ul>
      </section>
    </div>
    
    
    <div class="slide">
      <section class="header"><a name="additional" href="#additional"><h1> Example code and additional resources </h1></a></section>
      <section class="contents">
        <p><strong>Spring 2024 lecture:</strong></p>
        
        <ul>
            <!--<li><a href="">🎥 Class archive - Recursion (Spring 2024)</a></li>-->
            <li> WORK IN PROGRESS </li>
            <!-- <li><a href=""></a></li> -->
            <!-- 📺 Old video lecture -->
            <!-- 🎥 Class archive -->
            <!-- ⏲️ C++ in 60 seconds -->
        </ul>
        
        <p><strong>Archived videos and class lectures:</strong></p>
        <ul>
			<li> <a href="https://www.youtube.com/watch?v=74CZ3LmMgyU"> 🎥 Class archive - Smart Fixed-Length Array data structure (2022) </a> </li>
			<li> <a href="http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-16_cs250_lecture.mp4"> 🎥 Class archive - Smart Dynamic Array data structure PART 1 (2022) </a> </li>
			<li> <a href="http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-23_cs250_lectureA_smart_dynamic_array_addendum.mp4"> 🎥 Class archive - Smart Dynamic Array data structure PART 2 (2022) </a> </li>
        </ul>
      </section>
    </div>
    
  </section> 
  <!-- END OF presentation -->
  
  <script>
    function AutoPlayBegin()
    {
      PlaySlide( 1 );
    }
    
    function PlaySlide( page )
    {
      console.log( "Page:", page );
      location.replace( "#slide" + page );
      var time = ( document.getElementById( "slide" + page + "-audio" ).duration + 1 ) * 1000;
      console.log( "Duration:", time, "milliseconds" );
      document.getElementById( "slide" + page + "-audio" ).play();
      setTimeout( PlaySlide, time, page+1 );
    }
  </script>

</body>
